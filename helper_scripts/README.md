# Helperscripts 
The script `merge_spacetime_errornorms.py`
is part of the `../LDDsimulation/helpers.py` but has been adapted to manually
merge spacetime errornorm files that come out of mesh studies.
