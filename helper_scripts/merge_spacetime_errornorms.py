#!/usr/bin/python3
import csv

isRichards = {
    1: True,
    2: False,
    3: False,
    4: False,
    5: True,
    }

resolutions = {
                1: 5e-5,
                2: 5e-5,
                4: 5e-5,
                8: 5e-5,
                16: 4e-5,
                32: 4e-6,
                64: 3e-6,
                128: 2e-6,
                256: 1e-6,
                }

for subdomain, isR in isRichards.items():
    subdomain_has_phases = ["wetting"]
    if not isR:
        subdomain_has_phases = ["wetting", "nonwetting"]

    for phase in subdomain_has_phases:
        merge_filename = f"subdomain{subdomain}" + \
                                   f"-space-time-errornorm-{phase}-phase.csv"

        with open(merge_filename, 'w', newline='') as mergefile:
            mergefile_writer = csv.writer(mergefile, delimiter=' ',)
            # write header into merge five
            mergefile_writer.writerow(
                ['mesh_parameter', 'mesh_h', 'Linf', 'L1', 'L2']
                )
            for meshrez in resolutions.keys():
                input_filename = f"subdomain{subdomain}" + \
                                 f"-space-time-errornorm-{phase}-phase" + \
                                 f"_meshrez{meshrez}.csv"
                try:
                    print(f"found {input_filename}")
                    with open(input_filename, 'r', newline='') as inputfile:
                        inputfile_reader = csv.reader(inputfile, delimiter=' ')
                        # skip header
                        next(inputfile_reader)
                        for row in inputfile_reader:
                            mergefile_writer.writerow(row)
                except IOError:
                    print("File not accessible or nonexistant")
