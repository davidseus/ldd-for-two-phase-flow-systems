#!/bin/bash
# The user can pass the user and group id by passing
# --env HOST_UID=$(id -u $USER) --env HOST_GID=$(id -g $USER)
# with the UID on the host to the container. Should work for Linux, Mac and Windows.
# Allows to manage the writes for shared volumes.
if [ "$HOST_UID" ]; then
    usermod -u $HOST_UID fenics
fi
if [ "$HOST_GID" ]; then
    groupmod -g $HOST_GID fenics
fi
# Make sure that everything in /dumux is accessible by the dumux user
# sed "1d" removes first line which is the folder itself
# exclude the shared folder using grep. This folder should be accessible by setting the UID/GID.
# chown transfers ownership to group dumux user dumux
# the OR true trick avoids the script exiting due to use of set -e
# find /dumux -maxdepth 1 | sed "1d" | grep -v "/dumux/shared" | xargs chown -R dumux:dumux 2> /dev/null || true