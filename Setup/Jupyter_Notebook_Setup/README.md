# Instructions how to setup Jupyter Notebook
Jupyter Notebook was used at the beginning to try out certain things and playing around with sympy before certain 
aspects became incorporated into the code. So Jupyter notebook is not necessary to run an LDD simulation. 
In case you want to use it, here is how to set it up.
## Setup
Install virtualenv and pip. Then run

    virtualenv -p python3 ../.env
    source ../.env/bin/activate
    pip install -r requirements.txt

Install the RISE plugin within the current virtual environment

    jupyter-nbextension install rise --py --sys-prefix
    jupyter-nbextension enable rise --py --sys-prefix

Alternative without virtualenv: 

    python3 -m pip install --user -r requirements.txt

The installation should be located in ~/.local/bin. To be able to start your notebook with 'jupyter notebook', add ~/.local/bin your PATH-variable in  ~/.bashrc: 

    export PATH="~/.local/bin/:$PATH"

You might be needing the following two commands

    python3 -m pip install --upgrade --user setuptools pip
    python3 -m pip install --user tornado==4.5.3 

After running those, additionally install the RISE plugin like before

    jupyter-nbextension install rise --py --user
    jupyter-nbextension enable rise --py --user
