# Setup and installation instructions
The setup constists of cloning this repository, installing `docker` and `Fenics`,
installing a few python modules that are not shipped with the standard
distributions and hack away.
>>>
**Note:**
This code assumes that you run Linux. It has not been tested on Windows.
It has been tested on `Debian Buster` and `Archlinux`.
>>>

## Install docker
First, install `docker`, add yourelf to the `docker` group and run the
docker daemon. This may vary from distribution
to distribution. On Archlinux you can run

    sudo pacman -S docker

Check the name of the package for your distribution and the documentation of your
package manager to see how to install docker in your case.
Adding yourself to the docker group can by done by running

    sudo gpasswd -a <username> docker

where <username> is to be replaced by your user name. Don't forget to log out and back in again, to make
the change effective.

On `systemd`-based systems the daemon is started by running

    sudo systemctl start docker

in the command line.
>>>
**Note:**
Note: If you don't have admistrative rights on your system, ask your systemadmin
to perform these steps for you. The user that wants to run `docker` needs to
be in the docker group. If you have added yourself to the docker group, don't
forget to log out and back in again.
>>>
## Set up latest Fenix image in Docker
Pull the latest `Fenics` Docker image

    docker pull quay.io/fenicsproject/stable:latest

>>>
**Note:**
This code as been tested on the latest docker version, which is as of October
2020 `Fenics 2019`.
The above explains how to pull the latest `Fenics` docker image.
Drastic changes to Fenics are envisioned for the next release, notably
abandoning the mesh tool `mshr` which this code heavily relies on.
Therefore, before even starting, check on the `Fenics`
[website](https://fenicsproject.org/download/) if another version of `Fenics`
has come out.
If yes, you are free to check whether or not this code still works under the
latest version, but things might have broken.
In that case, the code needs to be adapted first.
Alternatively, look on
[the release cite](https://quay.io/repository/fenicsproject/stable?tab=tags)
for the latest 2019 tag and pull the 2019 image.
So assuming `2019.1.0` was the latest tag for the 2019 version, you would run

    docker pull quay.io/fenicsproject/stable:2019.1.0
>>>

## Create docker container named LDD-TPR with graphical display, share folder `pwd`
Move into the folder in which you cloned this directory and run

    docker run --dns=129.69.252.252 -it --env HOST_UID=$(id -u $USER) --env HOST_GID=$(id -g $USER) --env DISPLAY=unix$DISPLAY --device /dev/dri --volume /tmp/.X11-unix:/tmp/.X11-unix:rw --volume $(pwd):/home/fenics/shared --name LDD-TPR-fenics2019 quay.io/fenicsproject/stable:latest /bin/bash

This will setup your current folder as shared directory for the `Fenics` docker
image. Once you stepped into the docker image (see below), this shared
directory can be found in `~/shared`.
This directory is used to share files between your system and the docker
container. As you most likely will be programming on your system and only step
into the docker container to run a simulation, this is a necessary mechanism.

>>>
**Note:** If you pulled a version of docker with a fixed tag, you need to adapt
the line `quay.io/fenicsproject/stable:latest` in the above to
`quay.io/fenicsproject/stable:2019.1.0`.
>>>
If you want the container do be automatically deleted after exiting add `--rm` option.

If you have trouble with internet connection inside the container use

    --dns=YOUR_DNS_SERVER


## Start LDD-TPR container and step into the container
From anywhere in the command line you can start the docker container by running

    docker start LDD-TPR-fenics2019 & docker exec -ti -u fenics LDD-TPR-fenics2019 /bin/bash -l
>>>
Tip: This command should be stored as a script or alias to not have to remember it. For example, you could add
~~~bash
alias LDDdocker='docker start LDD-TPR-fenics2019 & docker exec -ti -u fenics LDD-TPR-fenics2019 /bin/bash -l'
~~~
to your ~/.bashrc file (outside of the Docker container).
>>>
You should see a welcome message greeting you
```
# FEniCS stable version image

Welcome to FEniCS/stable!

This image provides a full-featured and optimized build of the stable
release of FEniCS.

To help you get started this image contains a number of demo
programs. Explore the demos by entering the 'demo' directory, for
example:

    cd ~/demo/python/documented/poisson
    python3 demo_poisson.py
```

It is good practice to update the system in your `docker image` so run

    sudo apt-get update && sudo apt-get upgrade

Have a look around running `ls`. You should see the folder `shared` from where
you are, which should be the fenics home directoy.

## Install missing python3 modules
Once you are in the container navigate into the `Setup` folder of the cloned repository.
If you followed the steps above, this should be achieved by running

    cd shared/Setup

within the container. Upgrade the python package manager `pip` by running

    sudo pip3 install --upgrade pip

 and then run

    sudo pip3 install -r python_requirements.txt

to install missing python requirements needed to run `LDDsimulation` based
code.  In case you get an error

~~~
ERROR: After October 2020 you may experience errors when installing or updating packages. This is because pip will change the way that it resolves dependency conflicts.

We recommend you use --use-feature=2020-resolver to test your packages with the new resolver before it becomes the default.

pandas 1.1.3 requires numpy>=1.15.4, but you'll have numpy 1.13.3 which is incompatible.
~~~
you might have to run

    sudo pip3 --use-feature=2020-resolver install pandas

in this case and similary on offending packages listed in the warning.
In case you get a warning like this
~~~
  WARNING: The scripts f2py, f2py3 and f2py3.6 are installed in '/home/fenics/.local/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
~~~
you need to add
~~~bash
export PATH=/home/fenics/.local/bin:$PATH
~~~
to your `~/.bashrc` or `.profile` script within the `LDD-TPR` container.

Finally, add the `LDDsimulation` folder to your python path by adding
~~~bash
export PYTHONPATH=${PYTHONPATH}:${HOME}/shared/LDDsimulation/:
~~~
to `~/fenics.env.conf`. Failing to do so will result in complaints of missing
modules if you run an LDDsimulation based example. 
After steeing this you need to exit the container (by typing `exit`) and
step in again, to make changes effective.

### Testing your installation
Concrats, now you can test the installation by running the example
`./InstallationTest/installTest.py` from within your docker container.
Assuming you have stepped into your docker container (see above) do
~~~bash
cd ~/shared/Setup/InstallationTest
./run_simulation installTest.py
~~~
This should run the simulation and finish by an output similar to
~~~
[...]
Errornorms on subdomain1
phase: wetting
Linf: 0.0042076773235871145

L1: 0.00017973029458472718

L2: 0.0008131932461893378

Errornorms on subdomain2
phase: wetting
Linf: 0.002710740880961932

L1: 9.73312482410056e-05

L2: 0.00044527206568625654

phase: nonwetting
Linf: 0.0045331147006416215

L1: 0.00021445882146514718

L2: 0.0009595771245508499

fenics@bdac3735c78b:~/shared/Setup/InstallationTest$ exit
~~~

Alternatively you can run an example in the
[Usecases](../Usecases) folder of the `LDDsimulation` repo.

>>>
**Note:**
 It might happen, that not all usecases are running, due to forgetting to update
the script. If that happens and you wanted to run that particular usecase,
find a usecase that works and diff the two files, to see what's missing.
>>>
## Usefull docker commands

List all docker container

    docker ps -a


Remove container

    docker rm $container


Remove all stopped container

    docker container prune


List all images

    docker images


Remove image

    docker rmi $image


# Troubleshooting

**Problem**
1. I can't install packages via `apt-get install` inside the  container

2. I cannot create files or folders (no write permissions)

**Solution**

1. If the package is not found first `apt-get update` and then try again.
If there is no connection check your dns settings.

2. In the container, execute once the script `Rechtesetup/setpermissions.sh` to
gain write access in `/home/fenics/shared`

        cd /home/fenics/shared/Rechtesetup & sudo ./setpermissions.sh

## Usefull FEniCS Links

- The dolphin documentation including introductory documentation and demos
can be found here

    - [Dolphin (latest)](https://fenicsproject.org/docs/dolfin/latest/python/index.html)
    - [Dolfin (2019.1.0)](https://fenicsproject.org/docs/dolfin/2019.1.0/python/index.html)
    - [Dolfin (2018.1.0)](https://fenicsproject.org/docs/dolfin/2018.1.0/python/index.html)

It is sometimes usefull to look at older docs to find the right answers.

- Python API references:
An index for API references of dolphin (both cpp and python) can be found
[here](https://fenicsproject.org/docs/dolfin/).
The python API references can be found here

    - [Dolphin (Fenics latest)](https://fenicsproject.org/docs/dolfin/latest/python/api.html)
    - [Dolphin (Fenics 2019.1.0)](https://fenicsproject.org/docs/dolfin/2019.1.0/python/)
    - [Dolphin (Fenics 2018.1.0)](https://fenicsproject.org/docs/dolfin/2018.1.0/python/index.html)
    - [Dolphin (Fenics 2017.2.0)](https://fenicsproject.org/docs/dolfin/2017.2.0/python/programmers-reference/index.html)
>>>
**Tip:**
It is sometimes necessary to have a look at older versions because some details
are better explained there and the docs have not been updated yet.
>>>
- Fenics git in case you want to view the source files:

    https://bitbucket.org/fenics-project/


- Fenics based forums:

    https://www.allanswered.com/community/s/fenics-project/
