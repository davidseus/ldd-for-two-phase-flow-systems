"""Class domainSubstructuring and example substructurings, Copyright 2020, David Seus

Provides the base class and example classes for domain substructurings used
for LDD simulations within the LDDsimulation class.

Each class must provide geometric data of the simulation domain and its
substructuring in the form of lists of dolfin points (see below) and define the
setter functions
    self.__interface_def_points(self):
    self.__adjacent_subdomains(self):
    self.__subdomain_def_points(self):
    self.__outer_boundary_def_points(self):
which in turn populate the data structures
    self.interface_def_points
    self.adjacent_subdomains
    self.subdomain_def_points
    self.outer_boundary_def_points
that are then used in an instance of the LDDsimulation class to genererate
meshes, interfaces etc..
The structures
    self.interface_def_points
    self.adjacent_subdomains
    self.subdomain_def_points
are lists and
    self.outer_boundary_def_points
is a dictionary. In more detail, the data must be provided in the following way
(see also the class examples):

# GLOBAL SIMULATION DOMAIN AND SUBDOMAINS
self.subdomain_def_points is a list of dolphin point lists such that the first
(0th) entry contains the list of coordinates of the vertices of the GLOBAL
simulation domain followed by the lists of vertices of the subdomains.
In this way, the subdomain numbering starts with 1 and thus, the
list of vertices defining subdomain n is the one found in
self.subdomain_def_points[n].
Example (see twoSoilLayers class)
    self.subdomain_def_points = [
        self.__subdomain0_vertices, # vertices of global domain
        self.__subdomain1_vertices, # vertices of subdomain 1
        self.__subdomain2_vertices, # vertices of subdomain 2
        ]
An example of a list of vertices for the global domain:
    self.__subdomain0_vertices = [
        df.Point(-1.0, -1.0),
        df.Point(1.0, -1.0),
        df.Point(1.0, 1.0),
        df.Point(-1.0, 1.0)
        ]
This list defines a ploygone and must be specified in anticlockwise manner.
This is specific to Fenics' own mesh tool mshr, that has been used here.
If another mesh generator is to be used, these structures need to be revisited.

Similarly, self.interface_def_points is a list of dolfin point lists, such that
each list specifies the polygon that defines an interface between subdomains.
Example (see layeredSoil)
    self.interface_def_points = [
        self.__interface12_vertices,
        self.__interface23_vertices,
        self.__interface34_vertices,
        ]
where the list entries again are lists of dolfin points defining a polyogn
in the same way as self.__subdomain0_vertices.
Beware that sometimes interfaces need to be split into several parts if an
interface features an acute angle such that two faces of a mesh triangle
aligns with the same interface. (see layeredSoilInnerPatch example)

The list self.adjacent_subdomains specifies for each
interface the two neighbouring subdomains that share that particular interface.
The neigbours are specified as two-element lists and must be populated
according to the data in both self.subdomain_def_points and
self.interface_def_points:
Example (layeredSoil):
    self.adjacent_subdomains = [
        [1, 2],  # adjacent subdomains for interface defined by self.interface12_vertices
        [2, 3],  # adjacent subdomains for interface defined by self.interface23_vertices
        [3, 4],  # adjacent subdomains for interface defined by self.interface34_vertices
        ]
The order of neighbour pairs must align with the order in which the interfaces
have been specified in self.interface_def_points:
Neighbours of the interface specified
as k-th entry in the list self.interface_def_points must be given in the
k-th entry in self.adjacent_subdomains.
Similalry, the numbering of the subdomains is
defined by the order of entries in self.subdomain_def_points, i.e.
self.subdomain_def_points[0] is the global domain,
self.subdomain_def_points[1] is subdomain with index 1,
self.subdomain_def_points[2] is subdomain with index 2,
...
self.subdomain_def_points[k] is subdomain with index k,
This same subdomain numbering must be used when specifying neighbours in
self.adjacent_subdomains. Otherwise unpredictable behaviour may occur.

Lastly, self.outer_boundary_def_points is a dictionary containing
for each subdomain a dictionary of outer boundary parts again specified as
polygons.
Example (layeredSoil):
    self.outer_boundary_def_points = {
        # subdomain number: dictionary of polygons
        1: self.__subdomain1_outer_boundary_verts,
        2: self.__subdomain2_outer_boundary_verts,
        3: self.__subdomain3_outer_boundary_verts,
        4: self.__subdomain4_outer_boundary_verts,
    }
The keys of the dictionary are subdomain indices and each value is again
a dictionary containing pairs (boundary part number: dolphin point list)
specifying different parts of the outer boundary as polygons. The numbering
of the outer parts is irrelevant, but each subdomain can have no outer
boundary parts or one or several parts.
Example:
        self.__subdomain3_outer_boundary_verts = {
            0: [self.__interface34_vertices[4],
                self.__subdomain2_vertices[1]],
            1: [self.__subdomain2_vertices[0],
                self.__interface34_vertices[0]]
        }

        # if a subdomain has no outer boundary write None instead, i.e.
        # i: None
        self.__subdomain4_outer_boundary_verts = {
            0: [self.__subdomain4_vertices[6],
                self.__subdomain4_vertices[0],
                self.__subdomain4_vertices[1],
                self.__subdomain4_vertices[2]]
        }

# LICENSE #####################################################################
Copyright 2020, David Seus
david.seus[at]ians.uni-stuttgart.de
This file is part of the module LDDsimulation.

    LDDsimulation is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LDDsimulation is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LDDsimulation.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
"""
import dolfin as df
import helpers as hlp


class domainSubstructuring(object):
    """Base class for substructuring of a domain."""

    def __init__(self):
        """Set all variables."""
        hlp.print_once("\n DD Substructuring:\n")
        self.interface_def_points = None
        self.adjacent_subdomains = None
        self.subdomain_def_points = None
        self.outer_boundary_def_points = None

    def __interface_def_points(self):
        """Set self.interface_def_points."""
        raise(NotImplementedError())

    def __adjacent_subdomains(self):
        """Set self.adjacent_subdomains."""
        raise(NotImplementedError())

    def __subdomain_def_points(self):
        """Set self.subdomain_def_points."""
        raise(NotImplementedError())

    def __outer_boundary_def_points(self):
        """Set self.outer_boundary_def_points."""
        raise(NotImplementedError())


class globalDomain(domainSubstructuring):
    """layered soil substructuring with inner patch."""

    def __init__(self):
        """Layered soil case with inner patch."""
        super().__init__()
        hlp.print_once("\n Layered Soil with inner Patch:\n")
        # global domain
        self.__subdomain0_vertices = [
            df.Point(-1.0, -1.0),
            df.Point(1.0, -1.0),
            df.Point(1.0, 1.0),
            df.Point(-1.0, 1.0)
            ]

        self.__interface_def_points()
        self.__adjacent_subdomains()
        self.__subdomain_def_points()
        self.__outer_boundary_def_points()

    def __interface_def_points(self):
        """Set self.interface_def_points."""

        # interface_vertices introduces a global numbering of interfaces.
        self.interface_def_points = None

    def __adjacent_subdomains(self):
        """Set self.adjacent_subdomains."""
        self.adjacent_subdomains = None

    def __subdomain_def_points(self):
        """Set self.subdomain_def_points."""

        self.subdomain_def_points = [
            self.__subdomain0_vertices
            ]

    def __outer_boundary_def_points(self):
        """Set self.outer_boundary_def_points."""
        # vertex coordinates of the outer boundaries. If it can not be
        # specified as a polygon, use an entry per boundary polygon.
        # This information is used for defining the Dirichlet boundary
        # conditions. If a domain is completely internal, the
        # dictionary entry should be 0: None
        self.__subdomain0_outer_boundary_verts = {
            0: [self.__subdomain0_vertices[0],
                self.__subdomain0_vertices[1],
                self.__subdomain0_vertices[2],
                self.__subdomain0_vertices[3],
                self.__subdomain0_vertices[0]]
        }


        # if a subdomain has no outer boundary write None instead, i.e.
        # i: None
        # if i is the index of the inner subdomain.
        self.outer_boundary_def_points = {
            # subdomain number
            0: self.__subdomain0_outer_boundary_verts
        }

class twoSoilLayers(domainSubstructuring):
    """layered soil substructuring with two subdomains."""

    def __init__(self):
        """Layered soil case with two subdomains."""
        super().__init__()
        hlp.print_once("\n Layered Soil with two subdomains:\n")
        # global domain
        self.__subdomain0_vertices = [
            df.Point(-1.0, -1.0),
            df.Point(1.0, -1.0),
            df.Point(1.0, 1.0),
            df.Point(-1.0, 1.0)
            ]

        self.__interface_def_points()
        self.__adjacent_subdomains()
        self.__subdomain_def_points()
        self.__outer_boundary_def_points()

    def __interface_def_points(self):
        """Set self.interface_def_points."""
        self.__interface12_vertices = [
            df.Point(-1.0, 0.0),
            df.Point(1.0, 0.0)
            ]

        # interface_vertices introduces a global numbering of interfaces.
        self.interface_def_points = [
            self.__interface12_vertices,
            ]

    def __adjacent_subdomains(self):
        """Set self.adjacent_subdomains."""
        self.adjacent_subdomains = [
            [1, 2],
            ]

    def __subdomain_def_points(self):
        """Set self.subdomain_def_points."""
        # subdomain1.
        self.__subdomain1_vertices = [
            self.__interface12_vertices[0],
            self.__interface12_vertices[1],
            self.__subdomain0_vertices[2],
            self.__subdomain0_vertices[3]
            ]

        # subdomain2
        self.__subdomain2_vertices = [
            self.__subdomain0_vertices[0],
            self.__subdomain0_vertices[1],
            self.__interface12_vertices[1],
            self.__interface12_vertices[0]]

        self.subdomain_def_points = [
            self.__subdomain0_vertices,
            self.__subdomain1_vertices,
            self.__subdomain2_vertices,
            ]

    def __outer_boundary_def_points(self):
        """Set self.outer_boundary_def_points."""
        # vertex coordinates of the outer boundaries. If it can not be
        # specified as a polygon, use an entry per boundary polygon.
        # This information is used for defining the Dirichlet boundary
        # conditions. If a domain is completely internal, the
        # dictionary entry should be 0: None
        self.__subdomain1_outer_boundary_verts = {
            0: [self.__interface12_vertices[1],
                self.__subdomain0_vertices[2],
                self.__subdomain0_vertices[3],
                self.__interface12_vertices[0]]
        }

        self.__subdomain2_outer_boundary_verts = {
            0: [self.__interface12_vertices[0],
                self.__subdomain0_vertices[0],
                self.__subdomain0_vertices[1],
                self.__interface12_vertices[1]]
        }

        # if a subdomain has no outer boundary write None instead, i.e.
        # i: None
        # if i is the index of the inner subdomain.
        self.outer_boundary_def_points = {
            # subdomain number
            1: self.__subdomain1_outer_boundary_verts,
            2: self.__subdomain2_outer_boundary_verts,
        }


class layeredSoil(domainSubstructuring):
    """layered soil substructuring with inner patch."""

    def __init__(self):
        """Layered soil case with inner patch."""
        super().__init__()
        hlp.print_once("\n Layered Soil with inner Patch:\n")
        # global domain
        self.__subdomain0_vertices = [
            df.Point(-1.0, -1.0),
            df.Point(1.0, -1.0),
            df.Point(1.0, 1.0),
            df.Point(-1.0, 1.0)
            ]

        self.__interface_def_points()
        self.__adjacent_subdomains()
        self.__subdomain_def_points()
        self.__outer_boundary_def_points()

    def __interface_def_points(self):
        """Set self.interface_def_points."""
        self.__interface12_vertices = [
            df.Point(-1.0, 0.8),
            df.Point(0.3, 0.8),
            df.Point(0.5, 0.9),
            df.Point(0.8, 0.7),
            df.Point(1.0, 0.65)
            ]

        # interface23
        self.__interface23_vertices = [
            df.Point(-1.0, 0.0),
            # df.Point(-0.35, 0.0),
            # df.Point(0.0, 0.0),
            # df.Point(0.5, 0.0),
            # df.Point(0.85, 0.0),
            df.Point(1.0, 0.0)
            ]

        self.__interface34_vertices = [
            df.Point(-1.0, -0.6),
            df.Point(-0.6, -0.45),
            df.Point(0.3, -0.25),
            df.Point(0.65, -0.6),
            df.Point(1.0, -0.7)
            ]

        # interface_vertices introduces a global numbering of interfaces.
        self.interface_def_points = [
            self.__interface12_vertices,
            self.__interface23_vertices,
            self.__interface34_vertices,
            ]

    def __adjacent_subdomains(self):
        """Set self.adjacent_subdomains."""
        self.adjacent_subdomains = [
            [1, 2],
            [2, 3],
            [3, 4],
            ]

    def __subdomain_def_points(self):
        """Set self.subdomain_def_points."""
        self.__subdomain1_vertices = [
            self.__interface12_vertices[0],
            self.__interface12_vertices[1],
            self.__interface12_vertices[2],
            self.__interface12_vertices[3],
            self.__interface12_vertices[4],
            self.__subdomain0_vertices[2],
            self.__subdomain0_vertices[3]]

        self.__subdomain2_vertices = [
            self.__interface23_vertices[0],
            self.__interface23_vertices[1],
            self.__subdomain1_vertices[4],
            self.__subdomain1_vertices[3],
            self.__subdomain1_vertices[2],
            self.__subdomain1_vertices[1],
            self.__subdomain1_vertices[0]]

        self.__subdomain3_vertices = [
            self.__interface34_vertices[0],
            self.__interface34_vertices[1],
            self.__interface34_vertices[2],
            self.__interface34_vertices[3],
            self.__interface34_vertices[4],
            self.__subdomain2_vertices[1],
            self.__subdomain2_vertices[0]
            ]

        # subdomain3
        self.__subdomain4_vertices = [
            self.__subdomain0_vertices[0],
            self.__subdomain0_vertices[1],
            self.__subdomain3_vertices[4],
            self.__subdomain3_vertices[3],
            self.__subdomain3_vertices[2],
            self.__subdomain3_vertices[1],
            self.__subdomain3_vertices[0]
            ]

        self.subdomain_def_points = [
            self.__subdomain0_vertices,
            self.__subdomain1_vertices,
            self.__subdomain2_vertices,
            self.__subdomain3_vertices,
            self.__subdomain4_vertices,
            ]

    def __outer_boundary_def_points(self):
        """Set self.outer_boundary_def_points."""
        # vertex coordinates of the outer boundaries. If it can not be
        # specified as a polygon, use an entry per boundary polygon.
        # This information is used for defining the Dirichlet boundary
        # conditions. If a domain is completely internal, the
        # dictionary entry should be 0: None
        self.__subdomain1_outer_boundary_verts = {
            0: [self.__interface12_vertices[4],
                self.__subdomain0_vertices[2],
                self.__subdomain0_vertices[3],
                self.__interface12_vertices[0]]
        }

        self.__subdomain2_outer_boundary_verts = {
            0: [self.__interface23_vertices[1],
                self.__subdomain1_vertices[4]],
            1: [self.__subdomain1_vertices[0],
                self.__interface23_vertices[0]]
        }

        self.__subdomain3_outer_boundary_verts = {
            0: [self.__interface34_vertices[4],
                self.__subdomain2_vertices[1]],
            1: [self.__subdomain2_vertices[0],
                self.__interface34_vertices[0]]
        }

        # if a subdomain has no outer boundary write None instead, i.e.
        # i: None
        self.__subdomain4_outer_boundary_verts = {
            0: [self.__subdomain4_vertices[6],
                self.__subdomain4_vertices[0],
                self.__subdomain4_vertices[1],
                self.__subdomain4_vertices[2]]
        }

        # if i is the index of the inner subdomain.
        self.outer_boundary_def_points = {
            # subdomain number
            1: self.__subdomain1_outer_boundary_verts,
            2: self.__subdomain2_outer_boundary_verts,
            3: self.__subdomain3_outer_boundary_verts,
            4: self.__subdomain4_outer_boundary_verts,
        }


class layeredSoilInnerPatch(domainSubstructuring):
    """layered soil substructuring with inner patch."""

    def __init__(self):
        """Layered soil case with inner patch."""
        super().__init__()
        hlp.print_once("\n Layered Soil with inner Patch:\n")
        # global domain
        self.__subdomain0_vertices = [
            df.Point(-1.0, -1.0),
            df.Point(1.0, -1.0),
            df.Point(1.0, 1.0),
            df.Point(-1.0, 1.0)
            ]

        self.__interface_def_points()
        self.__adjacent_subdomains()
        self.__subdomain_def_points()
        self.__outer_boundary_def_points()

    def __interface_def_points(self):
        """Set self.interface_def_points."""
        self.__interface12_vertices = [
            df.Point(-1.0, 0.8),
            df.Point(0.3, 0.8),
            df.Point(0.5, 0.9),
            df.Point(0.8, 0.7),
            df.Point(1.0, 0.65)
            ]

        # interface23
        self.__interface23_vertices = [
            df.Point(-1.0, 0.0),
            df.Point(-0.35, 0.0),
            df.Point(0.0, 0.0)
            ]

        self.__interface24_vertices = [
            self.__interface23_vertices[2],
            df.Point(0.6, 0.0),
            ]

        self.__interface25_vertices = [
            self.__interface24_vertices[1],
            df.Point(1.0, 0.0)
            ]

        self.__interface32_vertices = [
            self.__interface23_vertices[2],
            self.__interface23_vertices[1],
            self.__interface23_vertices[0]
            ]

        self.__interface36_vertices = [
            df.Point(-1.0, -0.6),
            df.Point(-0.6, -0.45)
            ]

        self.__interface46_vertices = [
            self.__interface36_vertices[1],
            df.Point(0.3, -0.25)
            ]

        self.__interface56_vertices = [
            self.__interface46_vertices[1],
            df.Point(0.65, -0.6),
            df.Point(1.0, -0.7)
            ]

        self.__interface34_vertices = [
            self.__interface36_vertices[1],
            self.__interface23_vertices[2]
            ]

        # Interface 45 needs to be split, because of the shape. There can be
        # triangles with two facets on the interface and this creates a rogue
        # dof type error when integrating over that particular interface.
        # Accordingly, the lambda_param dictionary has two entries for that
        # interface.
        self.__interface45_vertices_a = [
            self.__interface56_vertices[0],
            df.Point(0.7, -0.2),
            ]

        self.__interface45_vertices_b = [
            df.Point(0.7, -0.2),
            self.__interface25_vertices[0]
            ]

        # interface_vertices introduces a global numbering of interfaces.
        self.interface_def_points = [
            self.__interface12_vertices,
            self.__interface23_vertices,
            self.__interface24_vertices,
            self.__interface25_vertices,
            self.__interface34_vertices,
            self.__interface36_vertices,
            self.__interface45_vertices_a,
            self.__interface45_vertices_b,
            self.__interface46_vertices,
            self.__interface56_vertices,
            ]

    def __adjacent_subdomains(self):
        """Set self.adjacent_subdomains."""
        self.adjacent_subdomains = [
            [1, 2],
            [2, 3],
            [2, 4],
            [2, 5],
            [3, 4],
            [3, 6],
            [4, 5],
            [4, 5],
            [4, 6],
            [5, 6]
            ]

    def __subdomain_def_points(self):
        """Set self.subdomain_def_points."""
        # subdomain1.
        self.__subdomain1_vertices = [
            self.__interface12_vertices[0],
            self.__interface12_vertices[1],
            self.__interface12_vertices[2],
            self.__interface12_vertices[3],
            self.__interface12_vertices[4],
            self.__subdomain0_vertices[2],
            self.__subdomain0_vertices[3]]

        # subdomain1
        self.__subdomain2_vertices = [
            self.__interface23_vertices[0],
            self.__interface23_vertices[1],
            self.__interface23_vertices[2],
            self.__interface24_vertices[1],
            self.__interface25_vertices[1],
            self.__subdomain1_vertices[4],
            self.__subdomain1_vertices[3],
            self.__subdomain1_vertices[2],
            self.__subdomain1_vertices[1],
            self.__subdomain1_vertices[0]]

        self.__subdomain3_vertices = [
            self.__interface36_vertices[0],
            self.__interface36_vertices[1],
            self.__interface34_vertices[1],
            self.__interface32_vertices[1],
            self.__interface32_vertices[2]
            ]

        # subdomain3
        self.__subdomain4_vertices = [
            self.__interface46_vertices[0],
            self.__interface46_vertices[1],
            self.__interface45_vertices_a[1],
            self.__interface24_vertices[1],
            self.__interface24_vertices[0],
            self.__interface34_vertices[1]
                   ]

        self.__subdomain5_vertices = [
            self.__interface56_vertices[0],
            self.__interface56_vertices[1],
            self.__interface56_vertices[2],
            self.__interface25_vertices[1],
            self.__interface25_vertices[0],
            self.__interface45_vertices_b[1],
            self.__interface45_vertices_b[0]
            ]

        self.__subdomain6_vertices = [
            self.__subdomain0_vertices[0],
            self.__subdomain0_vertices[1],
            self.__interface56_vertices[2],
            self.__interface56_vertices[1],
            self.__interface56_vertices[0],
            self.__interface36_vertices[1],
            self.__interface36_vertices[0]
            ]

        self.subdomain_def_points = [
            self.__subdomain0_vertices,
            self.__subdomain1_vertices,
            self.__subdomain2_vertices,
            self.__subdomain3_vertices,
            self.__subdomain4_vertices,
            self.__subdomain5_vertices,
            self.__subdomain6_vertices
            ]

    def __outer_boundary_def_points(self):
        """Set self.outer_boundary_def_points."""
        # vertex coordinates of the outer boundaries. If it can not be
        # specified as a polygon, use an entry per boundary polygon.
        # This information is used for defining the Dirichlet boundary
        # conditions. If a domain is completely internal, the
        # dictionary entry should be 0: None
        self.__subdomain1_outer_boundary_verts = {
            0: [self.__subdomain1_vertices[4],
                self.__subdomain1_vertices[5],
                self.__subdomain1_vertices[6],
                self.__subdomain1_vertices[0]]
        }

        self.__subdomain2_outer_boundary_verts = {
            0: [self.__subdomain2_vertices[9],
                self.__subdomain2_vertices[0]],
            1: [self.__subdomain2_vertices[4],
                self.__subdomain2_vertices[5]]
        }

        self.__subdomain3_outer_boundary_verts = {
            0: [self.__subdomain3_vertices[4],
                self.__subdomain3_vertices[0]]
        }

        self.__subdomain4_outer_boundary_verts = None

        self.__subdomain5_outer_boundary_verts = {
            0: [self.__subdomain5_vertices[2],
                self.__subdomain5_vertices[3]]
        }

        self.__subdomain6_outer_boundary_verts = {
            0: [self.__subdomain6_vertices[6],
                self.__subdomain6_vertices[0],
                self.__subdomain6_vertices[1],
                self.__subdomain6_vertices[2]]
        }

        # if a subdomain has no outer boundary write None instead, i.e.
        # i: None
        # if i is the index of the inner subdomain.
        self.outer_boundary_def_points = {
            # subdomain number
            1: self.__subdomain1_outer_boundary_verts,
            2: self.__subdomain2_outer_boundary_verts,
            3: self.__subdomain3_outer_boundary_verts,
            4: self.__subdomain4_outer_boundary_verts,
            5: self.__subdomain5_outer_boundary_verts,
            6: self.__subdomain6_outer_boundary_verts
        }


class layeredSoilInnerPatchStraight12(domainSubstructuring):
    """layered soil substructuring with inner patch."""

    def __init__(self):
        """Layered soil case with inner patch."""
        super().__init__()
        hlp.print_once("\n Layered Soil with inner Patch:\n")
        # global domain
        self.__subdomain0_vertices = [
            df.Point(-1.0, -1.0),
            df.Point(1.0, -1.0),
            df.Point(1.0, 1.0),
            df.Point(-1.0, 1.0)
            ]

        self.__interface_def_points()
        self.__adjacent_subdomains()
        self.__subdomain_def_points()
        self.__outer_boundary_def_points()

    def __interface_def_points(self):
        """Set self.interface_def_points."""
        self.__interface12_vertices = [
            df.Point(-1.0, 0.8),
            df.Point(0.3, 0.8),
            df.Point(0.5, 0.8),
            df.Point(0.8, 0.8),
            df.Point(1.0, 0.8)
            ]

        # interface23
        self.__interface23_vertices = [
            df.Point(-1.0, 0.0),
            df.Point(-0.35, 0.0),
            df.Point(0.0, 0.0)
            ]

        self.__interface24_vertices = [
            self.__interface23_vertices[2],
            df.Point(0.6, 0.0),
            ]

        self.__interface25_vertices = [
            self.__interface24_vertices[1],
            df.Point(1.0, 0.0)
            ]

        self.__interface32_vertices = [
            self.__interface23_vertices[2],
            self.__interface23_vertices[1],
            self.__interface23_vertices[0]
            ]

        self.__interface36_vertices = [
            df.Point(-1.0, -0.6),
            df.Point(-0.6, -0.45)
            ]

        self.__interface46_vertices = [
            self.__interface36_vertices[1],
            df.Point(0.3, -0.25)
            ]

        self.__interface56_vertices = [
            self.__interface46_vertices[1],
            df.Point(0.65, -0.6),
            df.Point(1.0, -0.7)
            ]

        self.__interface34_vertices = [
            self.__interface36_vertices[1],
            self.__interface23_vertices[2]
            ]

        # Interface 45 needs to be split, because of the shape. There can be
        # triangles with two facets on the interface and this creates a rogue
        # dof type error when integrating over that particular interface.
        # Accordingly, the lambda_param dictionary has two entries for that
        # interface.
        self.__interface45_vertices_a = [
            self.__interface56_vertices[0],
            df.Point(0.7, -0.2),
            ]

        self.__interface45_vertices_b = [
            df.Point(0.7, -0.2),
            self.__interface25_vertices[0]
            ]

        # interface_vertices introduces a global numbering of interfaces.
        self.interface_def_points = [
            self.__interface12_vertices,
            self.__interface23_vertices,
            self.__interface24_vertices,
            self.__interface25_vertices,
            self.__interface34_vertices,
            self.__interface36_vertices,
            self.__interface45_vertices_a,
            self.__interface45_vertices_b,
            self.__interface46_vertices,
            self.__interface56_vertices,
            ]

    def __adjacent_subdomains(self):
        """Set self.adjacent_subdomains."""
        self.adjacent_subdomains = [
            [1, 2],
            [2, 3],
            [2, 4],
            [2, 5],
            [3, 4],
            [3, 6],
            [4, 5],
            [4, 5],
            [4, 6],
            [5, 6]
            ]

    def __subdomain_def_points(self):
        """Set self.subdomain_def_points."""
        # subdomain1.
        self.__subdomain1_vertices = [
            self.__interface12_vertices[0],
            self.__interface12_vertices[1],
            self.__interface12_vertices[2],
            self.__interface12_vertices[3],
            self.__interface12_vertices[4],
            self.__subdomain0_vertices[2],
            self.__subdomain0_vertices[3]]

        # subdomain1
        self.__subdomain2_vertices = [
            self.__interface23_vertices[0],
            self.__interface23_vertices[1],
            self.__interface23_vertices[2],
            self.__interface24_vertices[1],
            self.__interface25_vertices[1],
            self.__subdomain1_vertices[4],
            self.__subdomain1_vertices[3],
            self.__subdomain1_vertices[2],
            self.__subdomain1_vertices[1],
            self.__subdomain1_vertices[0]]

        self.__subdomain3_vertices = [
            self.__interface36_vertices[0],
            self.__interface36_vertices[1],
            self.__interface34_vertices[1],
            self.__interface32_vertices[1],
            self.__interface32_vertices[2]
            ]

        # subdomain3
        self.__subdomain4_vertices = [
            self.__interface46_vertices[0],
            self.__interface46_vertices[1],
            self.__interface45_vertices_a[1],
            self.__interface24_vertices[1],
            self.__interface24_vertices[0],
            self.__interface34_vertices[1]
                   ]

        self.__subdomain5_vertices = [
            self.__interface56_vertices[0],
            self.__interface56_vertices[1],
            self.__interface56_vertices[2],
            self.__interface25_vertices[1],
            self.__interface25_vertices[0],
            self.__interface45_vertices_b[1],
            self.__interface45_vertices_b[0]
            ]

        self.__subdomain6_vertices = [
            self.__subdomain0_vertices[0],
            self.__subdomain0_vertices[1],
            self.__interface56_vertices[2],
            self.__interface56_vertices[1],
            self.__interface56_vertices[0],
            self.__interface36_vertices[1],
            self.__interface36_vertices[0]
            ]

        self.subdomain_def_points = [
            self.__subdomain0_vertices,
            self.__subdomain1_vertices,
            self.__subdomain2_vertices,
            self.__subdomain3_vertices,
            self.__subdomain4_vertices,
            self.__subdomain5_vertices,
            self.__subdomain6_vertices
            ]

    def __outer_boundary_def_points(self):
        """Set self.outer_boundary_def_points."""
        # vertex coordinates of the outer boundaries. If it can not be
        # specified as a polygon, use an entry per boundary polygon.
        # This information is used for defining the Dirichlet boundary
        # conditions. If a domain is completely internal, the
        # dictionary entry should be 0: None
        self.__subdomain1_outer_boundary_verts = {
            0: [self.__subdomain1_vertices[4],
                self.__subdomain1_vertices[5],
                self.__subdomain1_vertices[6],
                self.__subdomain1_vertices[0]]
        }

        self.__subdomain2_outer_boundary_verts = {
            0: [self.__subdomain2_vertices[9],
                self.__subdomain2_vertices[0]],
            1: [self.__subdomain2_vertices[4],
                self.__subdomain2_vertices[5]]
        }

        self.__subdomain3_outer_boundary_verts = {
            0: [self.__subdomain3_vertices[4],
                self.__subdomain3_vertices[0]]
        }

        self.__subdomain4_outer_boundary_verts = None

        self.__subdomain5_outer_boundary_verts = {
            0: [self.__subdomain5_vertices[2],
                self.__subdomain5_vertices[3]]
        }

        self.__subdomain6_outer_boundary_verts = {
            0: [self.__subdomain6_vertices[6],
                self.__subdomain6_vertices[0],
                self.__subdomain6_vertices[1],
                self.__subdomain6_vertices[2]]
        }

        # if a subdomain has no outer boundary write None instead, i.e.
        # i: None
        # if i is the index of the inner subdomain.
        self.outer_boundary_def_points = {
            # subdomain number
            1: self.__subdomain1_outer_boundary_verts,
            2: self.__subdomain2_outer_boundary_verts,
            3: self.__subdomain3_outer_boundary_verts,
            4: self.__subdomain4_outer_boundary_verts,
            5: self.__subdomain5_outer_boundary_verts,
            6: self.__subdomain6_outer_boundary_verts
        }


class chessBoardInnerPatch(domainSubstructuring):
    """layered soil substructuring with inner patch."""

    def __init__(self):
        """Layered soil case with inner patch."""
        super().__init__()
        hlp.print_once("\n Layered Soil with inner Patch:\n")
        # global domain
        self.__subdomain0_vertices = [
            df.Point(-1.0, -1.0),
            df.Point(1.0, -1.0),
            df.Point(1.0, 1.0),
            df.Point(-1.0, 1.0)
            ]

        self.__interface_def_points()
        self.__adjacent_subdomains()
        self.__subdomain_def_points()
        self.__outer_boundary_def_points()

    def __interface_def_points(self):
        """Set self.interface_def_points."""
        self.__interface23_vertices = [
            df.Point(0.0, -0.6),
            df.Point(0.7, 0.0)]

        self.__interface12_vertices = [
            self.__interface23_vertices[1],
            df.Point(1.0, 0.0)]

        self.__interface13_vertices = [
            df.Point(0.0, 0.0),
            self.__interface23_vertices[1]]

        self.__interface15_vertices = [
            df.Point(0.0, 0.0),
            df.Point(0.0, 1.0)]

        self.__interface34_vertices = [
            df.Point(0.0, 0.0),
            self.__interface23_vertices[0]]

        self.__interface24_vertices = [
            self.__interface23_vertices[0],
            df.Point(0.0, -1.0)]

        self.__interface45_vertices = [
            df.Point(-1.0, 0.0),
            df.Point(0.0, 0.0)]

        # interface_vertices introduces a global numbering of interfaces.
        self.interface_def_points = [
            self.__interface13_vertices,
            self.__interface12_vertices,
            self.__interface23_vertices,
            self.__interface24_vertices,
            self.__interface34_vertices,
            self.__interface45_vertices,
            self.__interface15_vertices,
            ]

    def __adjacent_subdomains(self):
        """Set self.adjacent_subdomains."""
        self.adjacent_subdomains = [
                [1, 3],
                [1, 2],
                [2, 3],
                [2, 4],
                [3, 4],
                [4, 5],
                [1, 5]
            ]

    def __subdomain_def_points(self):
        """Set self.subdomain_def_points."""
        # subdomain1.
        self.__subdomain1_vertices = [
            self.__interface23_vertices[0],
            self.__interface23_vertices[1],
            self.__interface12_vertices[1],
            self.__subdomain0_vertices[2],
            df.Point(0.0, 1.0)]

        # subdomain2
        self.__subdomain2_vertices = [
            self.__interface24_vertices[1],
            self.__subdomain0_vertices[1],
            self.__interface12_vertices[1],
            self.__interface23_vertices[1],
            self.__interface23_vertices[0]]

        self.__subdomain3_vertices = [
            self.__interface23_vertices[0],
            self.__interface23_vertices[1],
            self.__interface13_vertices[0]]

        self.__subdomain4_vertices = [
            self.__subdomain0_vertices[0],
            self.__interface24_vertices[1],
            self.__interface34_vertices[1],
            self.__interface34_vertices[0],
            self.__interface45_vertices[0]]

        self.__subdomain5_vertices = [
            self.__interface45_vertices[0],
            self.__interface15_vertices[0],
            self.__interface15_vertices[1],
            self.__subdomain0_vertices[3]]

        self.subdomain_def_points = [
            self.__subdomain0_vertices,
            self.__subdomain1_vertices,
            self.__subdomain2_vertices,
            self.__subdomain3_vertices,
            self.__subdomain4_vertices,
            self.__subdomain5_vertices,
            ]

    def __outer_boundary_def_points(self):
        """Set self.outer_boundary_def_points."""
        # vertex coordinates of the outer boundaries. If it can not be
        # specified as a polygon, use an entry per boundary polygon.
        # This information is used for defining the Dirichlet boundary
        # conditions. If a domain is completely internal, the
        # dictionary entry should be 0: None
        self.__subdomain1_outer_boundary_verts = {
            0: [self.__interface12_vertices[1],
                self.__subdomain0_vertices[2],
                df.Point(0.0, 1.0)]
            }

        self.__subdomain2_outer_boundary_verts = {
            0: [self.__interface24_vertices[1],
                self.__subdomain0_vertices[1],
                self.__interface12_vertices[1]]
            }

        self.__subdomain3_outer_boundary_verts = None

        self.__subdomain4_outer_boundary_verts = {
            0: [self.__interface45_vertices[0],
                self.__subdomain0_vertices[0],
                self.__interface24_vertices[1]]
        }

        self.__subdomain5_outer_boundary_verts = {
            0: [self.__interface15_vertices[1],
                self.__subdomain0_vertices[3],
                self.__interface45_vertices[0]]
        }

        # if a subdomain has no outer boundary write None instead, i.e.
        # i: None
        # if i is the index of the inner subdomain.
        self.outer_boundary_def_points = {
            1: self.__subdomain1_outer_boundary_verts,
            2: self.__subdomain2_outer_boundary_verts,
            3: self.__subdomain3_outer_boundary_verts,
            4: self.__subdomain4_outer_boundary_verts,
            5: self.__subdomain5_outer_boundary_verts,
        }
