"""helper functions for class LDDsimulation, Copyright 2020, David Seus
    Copyright partly by Lukas Ostrowski as indicated.

# LICENSE #####################################################################
Copyright 2020, David Seus
david.seus[at]ians.uni-stuttgart.de
This file is part of the module LDDsimulation.

    LDDsimulation is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LDDsimulation is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LDDsimulation.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
"""

import dolfin as df
import inspect
import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import typing as tp
import sympy as sym
import os
import pandas as pd
import LDDsimulation as ldd
import csv


# RUN #########################################################################
def run_simulation(
        parameter,
        processQueue,
        starttime: float = 0.0,
        mesh_resolution: float = 32,
        ) -> None:
    """Setup and run LDD simulation

    wrapper function to setup and run LDD simulation for starttime starttime
    and mesh resolution mesh_resolution.

    PARAMETERS
    starttime       #type float     number >= 0 specifying the starting point
                                    of the simulation.
    mesh_resolution #type float     mesh resolution parameter for Fenics.
    parameter       #type tp.Dict   Dictionary of all parameters of the LDD
                                    simulation.
    """
    # parameter = kwargs
    # initialise LDD simulation class
    simulation = ldd.LDDsimulation(
        tol=parameter["tol"],
        LDDsolver_tol=parameter["solver_tol"],
        debug=parameter["debugflag"],
        max_iter_num=parameter["max_iter_num"],
        FEM_Lagrange_degree=parameter["FEM_Lagrange_degree"],
        mesh_study=parameter["mesh_study"]
        )

    simulation.set_parameters(
        use_case=parameter["use_case"],
        output_dir=parameter["output_string"],
        subdomain_def_points=parameter["subdomain_def_points"],
        isRichards=parameter["isRichards"],
        interface_def_points=parameter["interface_def_points"],
        outer_boundary_def_points=parameter["outer_boundary_def_points"],
        adjacent_subdomains=parameter["adjacent_subdomains"],
        mesh_resolution=mesh_resolution,  # parameter["mesh_resolution"],
        viscosity=parameter["viscosity"],
        porosity=parameter["porosity"],
        L=parameter["L"],
        lambda_param=parameter["lambda_param"],
        relative_permeability=parameter["relative_permeability"],
        intrinsic_permeability=parameter["intrinsic_permeability"],
        saturation=parameter["sat_pressure_relationship"],
        starttime=starttime,  # parameter["starttime"],
        starttime_timestep_number_shift=parameter["starttime_timestep_number_shift"],
        number_of_timesteps=parameter["number_of_timesteps"],
        number_of_timesteps_to_analyse=parameter["number_of_timesteps_to_analyse"],
        plot_timestep_every=parameter["plot_timestep_every"],
        timestep_size=parameter["timestep_size"],
        sources=parameter["source_expression"],
        initial_conditions=parameter["initial_condition"],
        dirichletBC_expression_strings=parameter["dirichletBC"],
        exact_solution=parameter["exact_solution"],
        densities=parameter["densities"],
        include_gravity=parameter["include_gravity"],
        gravity_acceleration=parameter["gravity_acceleration"],
        write2file=parameter["write_to_file"],
        )

    simulation.initialise()
    output_dir = simulation.output_dir
    processQueue.put(output_dir)
    # simulation.write_exact_solution_to_xdmf()
    output = simulation.run(analyse_condition=parameter["analyse_condition"])

    for subdomain_index, subdomain_output in output.items():
        mesh_h = subdomain_output['mesh_size']
        for phase, error_dict in subdomain_output['errornorm'].items():
            filename = output_dir \
                + "subdomain{}".format(subdomain_index)\
                + "-space-time-errornorm-{}-phase_meshrez{}.csv".format(
                    phase,
                    mesh_resolution
                    )
            # for errortype, errornorm in error_dict.items():

            # eocfile = open("eoc_filename", "a")
            # eocfile.write( str(mesh_h) + " " + str(errornorm) + "\n" )
            # eocfile.close()
            # if subdomain.isRichards:mesh_h
            data_dict = {
                'mesh_parameter': mesh_resolution,
                'mesh_h': mesh_h,
            }
            for norm_type, errornorm in error_dict.items():
                data_dict.update(
                    {norm_type: errornorm}
                )
            errors = pd.DataFrame(data_dict, index=[mesh_resolution])
            # check if file exists
            if os.path.isfile(filename) is True:
                with open(filename, 'a') as f:
                    errors.to_csv(
                        f,
                        header=False,
                        sep='\t',
                        encoding='utf-8',
                        index=False
                        )
            else:
                errors.to_csv(
                    filename,
                    sep='\t',
                    encoding='utf-8',
                    index=False
                    )


def info(title):
    """show process ids of multiprocessing simulation

    Copyright 2019, Lukas Ostrowski
    """
    print(title)
    print('module name:', __name__)
    print('parent process:', os.getppid())
    print('process id:', os.getpid())


def print_once(*args,**kwargs):
    """When running in mpi only print arguments only once.

    Copyright 2019, Lukas Ostrowski
    """
    if df.MPI.rank(df.MPI.comm_world) == 0:
        print(*args,**kwargs)
        sys.stdout.flush()


def generate_exact_solution_expressions(
        symbols: tp.Dict[int, int],
        isRichards: tp.Dict[int, bool],
        symbolic_pressure: tp.Dict[int, tp.Dict[str, str]],
        symbolic_capillary_pressure: tp.Dict[int, str],
        viscosity: tp.Dict[int, tp.List[float]],
        porosity: tp.Dict[int, tp.Dict[str, float]],
        intrinsic_permeability: tp.Dict[int, float],
        relative_permeability: tp.Dict[int, tp.Dict[str, tp.Callable[...,None]] ],#
        relative_permeability_prime: tp.Dict[int, tp.Dict[str, tp.Callable[...,None]] ],
        saturation_pressure_relationship: tp.Dict[int, tp.Callable[...,None]] = None,#
        saturation_pressure_relationship_prime: tp.Dict[int, tp.Callable[...,None]] = None,#
        symbolic_S_pc_relationship: tp.Dict[int, tp.Callable[...,None]] = None,#
        symbolic_S_pc_relationship_prime: tp.Dict[int, tp.Callable[...,None]] = None,#
        densities: tp.Dict[int, tp.Dict[str, float]] = None,#
        gravity_acceleration: float = 9.81,
        include_gravity: bool = False,
    ) -> tp.Dict[str, tp.Dict[int, tp.Dict[str, str]] ]:
    """ Generate exact solution, source and initial condition code from symbolic expressions"""

    output = {
        'source': dict(),
        'initial_condition': dict(),
        'exact_solution': dict()
    }
    x = symbols["x"]
    y = symbols["y"]
    t = symbols["t"]
    # turn above symbolic code into exact solution for dolphin and
    # construct the rhs that matches the above exact solution.
    dtS = dict()
    div_flux = dict()
    if saturation_pressure_relationship is not None:
        S_pc_sym = saturation_pressure_relationship
        S_pc_sym_prime = saturation_pressure_relationship_prime
    for subdomain, isR in isRichards.items():
        dtS.update({subdomain: dict()})
        div_flux.update({subdomain: dict()})
        output['source'].update({subdomain: dict()})
        output['exact_solution'].update({subdomain: dict()})
        output['initial_condition'].update({subdomain: dict()})
        if isR:
            subdomain_has_phases = ["wetting"]
        else:
            subdomain_has_phases = ["wetting", "nonwetting"]

        # conditional for S_pc_prime
        pc = symbolic_capillary_pressure[subdomain]
        ki = intrinsic_permeability[subdomain]
        dtpc = sym.diff(pc, t, 1)
        dxpc = sym.diff(pc, x, 1)
        dypc = sym.diff(pc, y, 1)
        print(f" type of dtpc is {type(dtpc)}")
        if saturation_pressure_relationship is not None:
            S = sym.Piecewise((S_pc_sym[subdomain](pc), pc > 0), (1, True))
            dS = sym.Piecewise((S_pc_sym_prime[subdomain](pc), pc > 0), (0, True))
            print(f" type of dS is {type(dS)}")
        else:
            S = symbolic_S_pc_relationship[subdomain]
            dS = symbolic_S_pc_relationship_prime[subdomain]
            print(f" type of dS is {type(dS)}")
        for phase in subdomain_has_phases:
            # Turn above symbolic expression for exact solution into c code
            output['exact_solution'][subdomain].update(
                {phase: sym.printing.ccode(symbolic_pressure[subdomain][phase])}
                )
            # save the c code for initial conditions
            output['initial_condition'][subdomain].update(
                {phase: sym.printing.ccode(symbolic_pressure[subdomain][phase])}  #.subs(t, 0)
                )
            if phase == "nonwetting":
                dtS[subdomain].update(
                    {phase: -porosity[subdomain]*dS*dtpc}
                    )
            else:
                dtS[subdomain].update(
                    {phase: porosity[subdomain]*dS*dtpc}
                    )
            pa = symbolic_pressure[subdomain][phase]
            dxpa = sym.diff(pa, x, 1)
            dxdxpa = sym.diff(pa, x, 2)
            dypa = sym.diff(pa, y, 1)
            dydypa = sym.diff(pa, y, 2)
            mu = viscosity[subdomain][phase]
            ka = relative_permeability[subdomain][phase]
            dka = relative_permeability_prime[subdomain][phase]
            rho = densities[subdomain][phase]
            g = gravity_acceleration

            if phase == "nonwetting":
                # x part of div(flux) for nonwetting
                div_flux_x = -ki/mu*(-1*dka(1-S)*dS*dxpc*dxpa + dxdxpa*ka(1-S))
                # y part of div(flux) for nonwetting
                if include_gravity:
                    div_flux_y = -ki/mu*(-1*dka(1-S)*dS*dypc*(dypa + rho*g) + dydypa*ka(1-S))
                else:
                    div_flux_y = -ki/mu*(-1*dka(1-S)*dS*dypc*dypa + dydypa*ka(1-S))
            else:
                # x part of div(flux) for wetting
                div_flux_x = -ki/mu*(dka(S)*dS*dxpc*dxpa + dxdxpa*ka(S))
                # y part of div(flux) for wetting
                if include_gravity:
                    div_flux_y = -ki/mu*(dka(S)*dS*dypc*(dypa + rho*g) + dydypa*ka(S))
                else:
                    div_flux_y = -ki/mu*(dka(S)*dS*dypc*(dypa) + dydypa*ka(S))

            div_flux[subdomain].update({phase: div_flux_x + div_flux_y})
            contructed_rhs = dtS[subdomain][phase] + div_flux[subdomain][phase]
            output['source'][subdomain].update(
                {phase: sym.printing.ccode(contructed_rhs)}
                )

    return output


def generate_exact_DirichletBC(
        isRichards: tp.Dict[int, bool],
        outer_boundary_def_points: tp.Dict[int, tp.Dict[int, tp.List[df.Point]]],
        exact_solution: tp.Dict[int, tp.Dict[str, str]]
    ) -> tp.Dict[int, tp.Dict[str, str]]:
    """generate Dirichlet BC from exact solution"""
    dirichletBC = dict()
    # similarly to the outer boundary dictionary, if a patch has no outer boundary
    # None should be written instead of an expression.
    # This is a bit of a brainfuck:
    # dirichletBC[ind] gives a dictionary of the outer boundaries of subdomain ind.
    # Since a domain patch can have several disjoint outer boundary parts, the
    # expressions need to get an enumaration index which starts at 0.
    # So dirichletBC[ind][j] is the dictionary of outer dirichlet conditions of
    # subdomain ind and boundary part j.
    # Finally, dirichletBC[ind][j]['wetting'] and dirichletBC[ind][j]['nonwetting']
    # return the actual expression needed for the dirichlet condition for both
    # phases if present.

    # subdomain index: {outer boudary part index: {phase: expression}}
    for subdomain in isRichards.keys():
        # subdomain can have no outer boundary
        if outer_boundary_def_points[subdomain] is None:
            dirichletBC.update({subdomain: None})
        else:
            dirichletBC.update({subdomain: dict()})
            # set the dirichlet conditions to be the same code as exact solution on
            # the subdomain.
            for outer_boundary_ind in outer_boundary_def_points[subdomain].keys():
                dirichletBC[subdomain].update(
                    {outer_boundary_ind: exact_solution[subdomain]}
                    )

    return dirichletBC


def generate_exact_symbolic_pc(
                isRichards: tp.Dict[int, bool],
                symbolic_pressure: tp.Dict[int, tp.Dict[str, str]]
            ) -> tp.Dict[str, tp.Dict[str, str]]:
    """generate symbolic capillary pressure from exact solution"""

    symbolic_pc = dict()
    for subdomain, isR in isRichards.items():
        if isR:
            symbolic_pc.update(
                {subdomain: -symbolic_pressure[subdomain]['wetting'].copy()}
                )
        else:
            symbolic_pc.update(
                {subdomain: symbolic_pressure[subdomain]['nonwetting'].copy()
                            - symbolic_pressure[subdomain]['wetting'].copy()}
                )

    return symbolic_pc


def merge_spacetime_errornorms(isRichards: tp.Dict[int, bool],
                               resolutions: tp.Dict[int, float],
                               output_dir: str):
    """Merge spacetime error norms for parallel mesh study computation.

    Since spacetime errornorms for each mesh resolution get written into a
    separate file (to make mesh study computation parallelisable), they need to
    be merged for ease of plotting.
    """
    for subdomain, isR in isRichards.items():
        subdomain_has_phases = ["wetting"]
        if not isR:
            subdomain_has_phases = ["wetting", "nonwetting"]

        for phase in subdomain_has_phases:
            merge_filename = output_dir + f"subdomain{subdomain}" + \
                             f"-space-time-errornorm-{phase}-phase.csv"

            with open(merge_filename, 'w', newline='') as mergefile:
                mergefile_writer = csv.writer(mergefile, delimiter=' ',)
                # write header into merge five
                mergefile_writer.writerow(
                    ['mesh_parameter', 'mesh_h', 'Linf', 'L1', 'L2']
                    )
                for meshrez in resolutions.keys():
                    input_filename = output_dir + f"subdomain{subdomain}" + \
                                     f"-space-time-errornorm-{phase}-phase" + \
                                     f"_meshrez{meshrez}.csv"
                    try:
                        # print(f"found {input_filename}")
                        with open(input_filename, 'r', newline='') as inputfile:
                            inputfile_reader = csv.reader(inputfile,
                                                          delimiter=' '
                                                          )
                            # skip header
                            next(inputfile_reader)
                            for row in inputfile_reader:
                                mergefile_writer.writerow(row)
                    except IOError:
                        print("File not accessible or nonexistant")

    print("Merged all spacetime errornorm files")
