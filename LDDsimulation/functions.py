"""generation of data function dictionaries for LDDsimulation.

This file provides definitions of data functions such as relative
permeabilities and S-pc relationships used by LDDsimulation.
Data functions are defined as normal python functions and methods are provided
that return dictionaries containing these functions as pairs
(subdomain_index, func), where func has been turned into a function handle
at the aid of functools module. These dictionaries are input to the
LDDsimulation class.

# LICENSE #####################################################################
Copyright 2020, David Seus
david.seus[at]ians.uni-stuttgart.de
This file is part of the module LDDsimulation.

    LDDsimulation is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LDDsimulation is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LDDsimulation.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
"""
import typing as tp
import dolfin as df
import functools as ft

# RELATIVE PERMEABILITIES #####################################################
# Functions used as relative permeabilty functions for wetting phases
def Monomial(S, phase, N):
    if phase is "wetting":
        return S**N
    elif phase is "nonwetting":
        return (1-S)**N
    else:
        raise(NotImplementedError())

def Monomial_prime(S, phase, N):
    if phase is "wetting":
        return N*S**(N-1)
    elif phase is "nonwetting":
        return -N*(1-S)**(N-1)
    else:
        raise(NotImplementedError())


def vanGenuchtenMualem(S, phase, n_index):
    m = 1-1/n_index
    if phase is "wetting":
        return S**(1/2)*(1-(1-S**(1/m))**m)**2
    elif phase is "nonwetting":
        return (1-S)**(1/3)*(1-S**(1/m))**(2*m)
    else:
        raise(NotImplementedError())


def vanGenuchtenMualem_prime(S, phase, n_index):
    m = 1-1/n_index
    if phase is "wetting":
        part1 = S**(1/2)
        part1_prime = 1/2*S**(-1/2)
        part2 = (1-(1-S**(1/m))**m)**2
        part2_prime = 2*(1-(1-S**(1/m))**m)*(-m*(1-S**(1/m))**(m-1))*(-1/m*S**(1/m-1))
        return part1_prime*part2 + part1*part2_prime
    elif phase is "nonwetting":
        part1 = (1-S)**(1/3)
        part1_prime = -1/3*(1-S)**(-2/3)
        part2 = (1-S**(1/m))**(2*m)
        part2_prime = 2*m*(1-S**(1/m))**(2*m-1)*(-1/m*S**(1/m-1))
        return part1_prime*part2 + part1*part2_prime
    else:
        raise(NotImplementedError())


def generate_relative_permeability_dicts(
        rel_perm_definition: tp.Dict[int, tp.Dict[str,str]]
        ) -> tp.Dict[str, tp.Dict[int,tp.Dict[str, tp.Callable]]]:
    """Generate permeabilty dictionary from definition dict.

    Generate permeabilty dictionary from input definition dictionary
    rel_perm_definition. This dictionary contains for each subdomain a
    dictionary which in turn contains for each phase a descriptive string
    describing which function should be used as relative permeabilty for that
    particular phase. The supported cases are defined by the if statements
    below.
    The output is a dictionary containing both the relative permeability
    function dictionaries (output["ka"]) as well as their corresponding
    derivative dictionaries (output["ka_prime"]) for each subdomain.
    """
    output = dict()
    output.update({"ka": dict()})
    output.update({"ka_prime": dict()})
    for subdomain, ka_dicts in rel_perm_definition.items():
        output["ka"].update({subdomain: dict()})
        output["ka_prime"].update({subdomain: dict()})
        for phase, function_type in ka_dicts.items():
            # the following isinstance handling is implemented to keep older
            # uscase scripts running withould having to reimplement the definiton
            # of the relative permeabilties. The more flexible way should be the
            # one implemented in the isinstance(function_type, dict) way.
            if isinstance(function_type, str):
                if function_type is "Spow2":
                    output["ka"][subdomain].update(
                        {phase: ft.partial(Monomial, phase="wetting", N=2)}
                        )
                    output["ka_prime"][subdomain].update(
                        {phase: ft.partial(Monomial_prime, phase="wetting", N=2)}
                        )
                elif function_type is "oneMinusSpow2":
                    output["ka"][subdomain].update(
                        {phase: ft.partial(Monomial, phase="nonwetting", N=2)}
                        )
                    output["ka_prime"][subdomain].update(
                        {phase: ft.partial(Monomial_prime, phase="nonwetting", N=2)}
                        )
                elif function_type is "Spow3":
                    output["ka"][subdomain].update(
                        {phase: ft.partial(Monomial, phase="wetting", N=3)}
                        )
                    output["ka_prime"][subdomain].update(
                        {phase: ft.partial(Monomial_prime, phase="wetting", N=3)}
                        )
                elif function_type is "oneMinusSpow3":
                    output["ka"][subdomain].update(
                        {phase: ft.partial(Monomial, phase="nonwetting", N=3)}
                        )
                    output["ka_prime"][subdomain].update(
                        {phase: ft.partial(Monomial_prime, phase="nonwetting", N=3)}
                        )
                else:
                    raise(NotImplementedError())
            elif isinstance(function_type, dict):
                # this is the way things should be implemented.
                for type, parameter in function_type.items():
                    if type is "monomial":
                        output["ka"][subdomain].update(
                            {phase: ft.partial(Monomial, phase=phase, N=parameter["power"])}
                            )
                        output["ka_prime"][subdomain].update(
                            {phase: ft.partial(Monomial_prime, phase=phase, N=parameter["power"])}
                            )
                    elif type is "vanGenuchtenMualem":
                        output["ka"][subdomain].update(
                            {phase: ft.partial(vanGenuchtenMualem, phase=phase, n_index=parameter["n"])}
                            )
                        output["ka_prime"][subdomain].update(
                            {phase: ft.partial(vanGenuchtenMualem_prime, phase=phase, n_index=parameter["n"])}
                            )
                    else:
                        raise(NotImplementedError())

            else:
                raise(NotImplementedError())
    return output

# S-Pc RELATIONSHIPS ##########################################################
def test_S(pc, index):
    """Inverse capillary pressure-saturation-relationship.

    Inverse capillary pressure-saturation-relationship that will
    be used by the simulation class
    this function needs to be monotonically decreasing in the
    capillary_pressure. Since in the richards case pc=-pw, this
    becomes as a function of pw a monotonically INCREASING function
    like in our Richards-Richards paper.
    However since we unify the treatment in the
    code for Richards and two-phase, we need the same requierment
    for both cases, two-phase and Richards.
    """
    # inverse capillary pressure-saturation-relationship
    return df.conditional(pc > 0, 1/((1 + pc)**(1/(index + 1))), 1)

def test_S_sym(pc, index):
    """Inverse capillary pressure-saturation-relationship.

    Inverse capillary pressure-saturation-relationship as symbolic
    expression, that will be used by
    helpers.generate_exact_solution_expressions()
    """
    # inverse capillary pressure-saturation-relationship
    return 1/((1 + pc)**(1/(index + 1)))

# derivative of S-pc relationship with respect to pc.
# This is needed for the construction of a analytic solution.
def test_S_prime_sym(pc, index):
    """Derivative of inverse pc-S-relationship.

    Derivative of the inverse pc-S-relationship as symbolic
    expression, that will be used by
    helpers.generate_exact_solution_expressions()
    """
    # inverse capillary pressure-saturation-relationship
    return -1/((index+1)*(1 + pc)**((index+2)/(index+1)))

# Van Genchuten S-pc relationship
# We use the van Genuchten approach, i.e. pc = 1/alpha*(S^{-1/m} -1)^1/n, where
# assume m = 1-1/n (see Helmig) and assume that residual saturation is Sw=0
def vanGenuchten_S(pc, n_index, alpha):
    # inverse capillary pressure-saturation-relationship for the simulation
    vanG = (1.0/((1 + (alpha*pc)**n_index))**((n_index - 1)/n_index))
    return df.conditional(pc > 0, vanG, 1)

def vanGenuchten_S_sym(pc, n_index, alpha):
    # inverse capillary pressure-saturation-relationship
    return (1.0/((1 + (alpha*pc)**n_index))**((n_index - 1)/n_index))

# derivative of S-pc relationship with respect to pc. This is needed for the
# construction of a analytic solution.
def vanGenuchten_S_prime_sym(pc, n_index, alpha):
    # inverse capillary pressure-saturation-relationship
    enumerator = -1.0*(alpha*(n_index - 1)*(alpha*pc)**(n_index - 1))
    denominator = ((1 + (alpha*pc)**n_index)**((2*n_index - 1)/n_index))
    return enumerator/denominator

def generate_Spc_dicts(
        Spc_on_subdomains: tp.Dict[int, tp.Dict[str, tp.Dict[str, float]]]
        )-> tp.Dict[str, tp.Dict[int, tp.Callable]]:
    """Generate S-pc dictionaries from definition dict.

    Generate S-pc dictionaries from input definition dictionary
    Spc_on_subdomains. This dictionary contains for each subdomain a
    dictionary which in turn contains as key a descriptive string
    describing which function should be used as S-pc relation for that
    particular subdomain. The values are parameters for that function type
    e.g. in the case of Van Genuchten or Brooks and Correy.
    The supported cases are defined by the if statements
    below.
    The output is a dictionary containing three dictionaries, the S-pc
    relationship for the simulation class as well as a symbolic version and
    its derivative for exact solution generation.
    """
    output = dict()
    output.update({"symbolic": dict()})
    output.update({"prime_symbolic": dict()})
    output.update({"dolfin": dict()})
    for subdomain, Spc_dict in Spc_on_subdomains.items():
        for Spc_type, parameters in Spc_dict.items():
            if Spc_type == "testSpc":
                output["symbolic"].update(
                    {subdomain: ft.partial(
                        test_S_sym,
                        index=parameters["index"]
                        )},
                )
                output["prime_symbolic"].update(
                    {subdomain: ft.partial(
                        test_S_prime_sym,
                        index=parameters["index"]
                        )},
                )
                output["dolfin"].update(
                    {subdomain: ft.partial(
                        test_S,
                        index=parameters["index"]
                        )},
                )
            elif Spc_type == "vanGenuchten":
                output["symbolic"].update(
                    {subdomain: ft.partial(
                        vanGenuchten_S_sym,
                        n_index=parameters["n"],
                        alpha=parameters["alpha"]
                        )},
                )
                output["prime_symbolic"].update(
                    {subdomain: ft.partial(
                        vanGenuchten_S_prime_sym,
                        n_index=parameters["n"],
                        alpha=parameters["alpha"]
                        )},
                )
                output["dolfin"].update(
                    {subdomain: ft.partial(
                        vanGenuchten_S,
                        n_index=parameters["n"],
                        alpha=parameters["alpha"]
                        )},
                )
            else:
                raise(NotImplementedError())

    return output
