""" Solution File class for LDDsimulation, Copyright (c) 2017 Alexander G. Zimmerman

This class was adpted from Lucas Ostrowski, which in turn adapted from
Copyright (c) 2017 Alexander G. Zimmerman under MIT license,
see # https://github.com/geo-fluid-dynamics/phaseflow-fenics

# LICENSE #####################################################################
Copyright 2020, David Seus
david.seus[at]ians.uni-stuttgart.de
Copyright (c) 2017 Alexander G. Zimmerman
This file is part of the module LDDsimulation.

    LDDsimulation is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LDDsimulation is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LDDsimulation.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
"""

import dolfin as df
# https://github.com/geo-fluid-dynamics/phaseflow-fenics
# adapted from Lucas Ostrowski
class SolutionFile(df.XDMFFile):
    """
    This class extends `df.XDMFFile` with some minor changes for convenience.
    """
    def __init__(self, mpicomm, filepath):
        df.XDMFFile.__init__(self, mpicomm, filepath)

        self.parameters["functions_share_mesh"] = True
        self.parameters["flush_output"] = True
        self.path = filepath # Mimic the file path attribute from a `file` returned by `open`
