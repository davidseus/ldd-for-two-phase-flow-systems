# Usecases

In this folder and its various subfolders several usecases of LDD simulations
can be found.
The structure is as follows.

- [Richards-Richards](./Richards-Richards): domain decomposition examples using
  only the Richards model.
- [Two-Phase-Richards](./Two-Phase-Richards): domain decomposition examples using a flexible combination of the Richards and two-phase flow model.
- [Two-Phase-Two-Phase](./Two-Phase-Two-Phase): domain decomposition examples using only the two-phase flow model.

Each of the folders contains subfolders such as `one-patch`, `two-patch` and `multi-patch` holding domain decomposition examples featuriing one-patch, two-patch or multi-patch substructurings. This ordering has been introduced to
keep things tidy. You may introduce your own ordering, of course.

Each usecase is a python script making use of the code in `../LDDsimulation`
to setup and run a simulation.
The different usecases could have been distilled in a usecase class to increase
maintainability. This was desided against mostely due to time contstraints.
If you plan on expand on this code, it might be a good idea to think about
a usecase class to tidy up the usecases even further. 
>>>
**!Waring!**
The usecases in the various `Archive` folders are obsolete.
They were used at one point in time in the development
of the LDD code usually for debugging but have been abandoned at one point in
time.
This means that the scripts are not up to date and up to par with the qualtiy
in the non-Archive folders. Most likely, they will not work anymore.

In some cases these scripts are being kept for reference or because older
simulation datat based on the older scripts have been used.
If you want to revive some of these examples, copy one of the official examples
and update that copy with information in the obsolete scripts.
>>>

## How to run a simulation

In each usecase folder you should see a script `run_simulation` wich takes one
mandatory and one optional parameter. Say we want to run the script
`./Two-Phase-Richards/two-patch/TP-R-two-patch-test-case/TP-R-2-patch-realistic.py`
we would start the `LDD-TPR` docker container (see `../Setup`), navigate into
the directory
~~~bash
cd ~/share/Two-Phase-Richards/two-patch/TP-R-two-patch-test-case/
~~~
and then run the simulation by issuing
~~~bash
./run_simulation TP-R-2-patch-realistic.py
~~~
This will run the simulation and save the output of `stdout` in a logfile called
`TP-R-2-patch-realistic.log`. Optionally, you can specify the name of the logfile
like so
~~~bash
./run_simulation TP-R-2-patch-realistic.py <my-fancy-log-file-name>
~~~
This will run the simulation and save the output of `stdout` to a file named
`my-fancy-log-file-name`.

## How to create your own usecases

In order to create a new usecase, either modify one of the existing ones, or
copy one of the existing cases into a new file and start working on your own.
>>>
**Note:**
 It might happen, that not all usecases are running, due to forgetting to update
the script. If that happens and you wanted to run that particular usecase,
find a usecase that works and diff the two files, to see what's missing.
Usually a parameter that has been later on during development is missing so
that the simulation complains of missing parameters or unknown variables.
**Sigh**. It is what it is.
>>>

To understand how usecases are set up, let's have a look at the file
`TP-R-2-patch-test.py` in
[Two-Phase-Richards/two-patch/TP-R-two-patch-test-case/](./Two-Phase-Richards/two-patch/TP-R-two-patch-test-case/).
Each script is broken into certain parts, which we explain in what follows.

After the license notice and an importblock, the first block is

### PREREQUISITS
~~~python
# PREREQUISITS  ###############################################################
# check if output directory "./output" exists. This will be used in
# the generation of the output string.
if not os.path.exists('./output'):
    os.mkdir('./output')
    print("Directory ", './output',  " created ")
else:
    print("Directory ", './output',  " already exists. Will use as output \
    directory")

date = datetime.datetime.now()
datestr = date.strftime("%Y-%m-%d")

# Name of the usecase that will be printed during simulation.
use_case = "TPR-2-patch-realistic-testrun"
# The name of this very file. Needed for creating log output.
thisfile = "TP-R-2-patch-test.py"
~~~
The PREREQUISITS block contains a routine that checks whether or not the
directory  `output/`  in the same folder than the script exists and creates it
in case it doesn't.
The two variables that should be eddited by the programmer are `use_case`
and `thisfile`.
- `use_case`  contains the name of the name of your usecase. This name is
used for `stdout`, but also for creating the output directory for the
simulation data. Chose a descriptive name for the usecase you are simulating.!
- `thisfile` should be the name of the script file. This variable is used to
output the content of the script to stdout and into the logfile if the script
`run_simulation` is used to start the simulation. The latter should be
contained in all folders.

### SOLVER CONFIG
~~~python
# GENERAL SOLVER CONFIG  ######################################################
# maximal iteration per timestep
max_iter_num = 500
FEM_Lagrange_degree = 1
~~~
This block sets the maximal number of L-iterations `max_iter_num` that the
LDD solver gets for the calculation of each time step.
`FEM_Lagrange_degree` sets the degree of FEM ansatz functions that are used.
While the communication of dofs over interfaces has been writen in a way to
allow for general anasatz functions, other parts of the code have not.
If higher order FEM methods are to be used, the code will have to be adjusted.

### GRID AND MESH STUDY CONFIG
~~~python
# GRID AND MESH STUDY SPECIFICATIONS  #########################################
mesh_study = True
resolutions = {
                # 1: 1e-6,
                # 2: 1e-6,
                # 4: 1e-6,
                # 8: 1e-6,
                # 16: 5e-6,
                # 32: 5e-6,
                64: 2e-6,
                # 128: 1e-6,
                # 256: 1e-6,
                }

# starttimes gives a list of starttimes to run the simulation from.
# The list is looped over and a simulation is run with t_0 as initial time
#  for each element t_0 in starttimes.
starttimes = {0: 0.0}
timestep_size = 0.001
number_of_timesteps = 20
~~~
This block sets mesh parameters, number of timesteps to be simulated, timestep
size as well as whether or not a mesh study is being performed.
In detail:
- `mesh_study = True/False`: Toggle mesh study mode.
  This changes the output slightly. Have a look at examples in the `mesh_study`
  folders. If the parameters is set to `True` the word mesh_study is appended
  to the output directory and all mesh sizes are output into the same
  directory.
  If set to `False`, data for each mesh size gets its own directory.
- `resolutions`: is a dictionary containing pairs `mesh_resolution: error_tol`.
  The key of each pair, `mesh_resolution`, is the `mesh_resolution` parameter for `mshr` determing the mesh size h and the
  value `error_tol` is the error tolerance that is used to stop the iteration. The error criterion is the L^2-norm of subsequent iterates. Once all subsequent errors on all subdomains are lower than `error_tol`, the
  LDD solver stops and the calculation of the time step is considerd finished.
  Doubling the `mesh_resolution` parameter should result in halfing the grid
  width. This is dependent on the geometry and is not always true for the first
  few values given in the list above. This is a `mshr` issue and cannot be
  remidied without changing the mesh generation to another mesh generator.
  If more than one entry is given in `resolutions`, all pairs are being calculated in parallel. This is usefull if one is interested in the results
  of the same usecase but calcuated for different mesh sizes h.
  Notably, if `mesh_study` is `True`you should have most of the above commented out, and data for a mesh study is performed. Notably space-time errornorms are put out.

  ~~~
  **!Warning!**
  Notably the last to pairs take a long time to calculate and memory shortage
  might be an issue depending on your machine.
  ~~~

- `starttimes = {0: 0.0}`: `starttimes` is a dictionary containing pairs
  `t0_index: t0` specifying starttimes `t0` along with the number this start  time should be given `t0_index`. Usually you will want to have        `t0_index`
  set to zero because `t0` is the intial timestep.
  However, if the simulation stopped for some reason and part of the data is
  valid, a later time can be specified along with the index of the timestep
  it had in the first attempt of the simulation.
  **Example:** Say you wanted start the simulation at `t0 = 0.5` and let that be the 87th timestep, you would set `starttimes = {87: 0.5}`.
  In case more than one element is given parallel simulations are started starting from the specified starttimes.
  **Example:**
  Assume `timestep_size = 0.01` and `number_of_timesteps = 50`
  `starttimes = {
    0: 0.0,
    50: 0.5
  }`
  yields a simulation over [0.0,1.0] but split into to processes and saved into
  two different folders.  
  This is also usefull to test parameters if `number_of_timesteps = 1` and
  the behaviour of the solver want to be tested at various timesteps one
  could set up an example as
  ~~~python
  starttimes = {
    0: 0.0,
    1: 0.5,
    2: 1.0
    3: 1.5
  }
  ~~~
  to test the behaviour of the solver at different times.

- `timestep_size`: Set size of the timestep.
- `number_of_timesteps`: Set numbers of timesteps to calculate.

### LDD SCHEME PARAMETERS
~~~python
# LDD scheme parameters  ######################################################
Lw1 = 0.25
Lnw1= 0.25

Lw2 = 0.5
Lnw2= 0.25

lambda_w = 40
lambda_nw = 40

include_gravity = True
debugflag = True
analyse_condition = False
~~~
This block sets parameters for the LDD solver. Namely L and λ.
- `L`: Each subdomain (say with index k) should get up to a pair of L parameters
one for each phase, i.e. `Lwk =` in the case of Richards and `Lwk =, Lnwk =` if two-phase flow is assumed.

- `λ`: Similary, each interfaces should get a pair of λ values. Since in the above example we only have one interface, we only get one pair of λ values.
See multi-domain examples for a generalisation.

- `include_gravity = True/False`: toggle the inclusion of gravity.
- `debugflag = True/False`: toggle debugging output.
- `analyse_condition = False/False`: toggle the inclusion of condition number analysis. Setting this setting to `True` might impact performance since the calculation of condition numbers can be quite time consuming.

### I/O CONFIG
~~~python
# I/O CONFIG  #################################################################
# when number_of_timesteps is high, it might take a long time to write all
# timesteps to disk. Therefore, you can choose to only write data of every
# plot_timestep_every timestep to disk.
plot_timestep_every = 4
# Decide how many timesteps you want analysed. Analysed means, that
# subsequent errors of the L-iteration within the timestep are written out.
number_of_timesteps_to_analyse = 5

# fine grained control over data to be written to disk in the mesh study case
# as well as for a regular simuation for a fixed grid.
if mesh_study:
    write_to_file = {
        # output the relative errornorm (integration in space) w.r.t. an exact
        # solution for each timestep into a csv file.
        'space_errornorms': True,
        # save the mesh and marker functions to disk
        'meshes_and_markers': True,
        # save xdmf/h5 data for each LDD iteration for timesteps determined by
        # number_of_timesteps_to_analyse. I/O intensive!
        'L_iterations_per_timestep': False,
        # save solution to xdmf/h5.
        'solutions': True,
        # save absolute differences w.r.t an exact solution to xdmf/h5 file
        # to monitor where on the domains errors happen
        'absolute_differences': True,
        # analyise condition numbers for timesteps determined by
        # number_of_timesteps_to_analyse and save them over time to csv.
        'condition_numbers': analyse_condition,
        # output subsequent iteration errors measured in L^2  to csv for
        # timesteps determined by number_of_timesteps_to_analyse.
        # Usefull to monitor convergence of the acutal LDD solver.
        'subsequent_errors': True
    }
else:
    write_to_file = {
        'space_errornorms': True,
        'meshes_and_markers': True,
        'L_iterations_per_timestep': False,
        'solutions': True,
        'absolute_differences': True,
        'condition_numbers': analyse_condition,
        'subsequent_errors': True
    }
~~~
This block sets up several options for outputting data. Control is given over
how many timesteps are to be analysed and whether every timestep or every kth
timestep should be written out.
In this way performance can be optimised and data size can be managed.
- `plot_timestep_every`: When `number_of_timesteps` is high, i.e. a high number
of timesteps are to be calculated, writing all data out to disk every timestep
can be time consuming. Therefore, `plot_timestep_every` sets the simulator to only write data of every plot_timestep_every -th timestep to disk.

- `number_of_timesteps_to_analyse`: Decide how many timesteps you want analysed. Analysed means, that subsequent errors of the L-iteration within the timestep are written out and some processing is done within the timesteps to
analyse what's going on. This number splits the simulation interval into intervals of equal length such that `number_of_timesteps_to_analyse` timesteps
are written out.   

- `write_to_file`: Dictionary setting which data is to be written out. This can be adjusted depending on the `mesh_study` parameter.
Various parameters are described in the comment.

### OUTPUT FILE STRING
~~~python
# OUTPUT FILE STRING  #########################################################
output_string = "./output/{}-{}_timesteps{}_P{}".format(
    datestr, use_case, number_of_timesteps, FEM_Lagrange_degree
    )
~~~
This block influences the naming of the output directory. Unless you are unhappy with the naming scheme of output directories, you don't need to touch this block on a per usecase basis.

### DOMAIN SUBSTRUCTURING
~~~python
# DOMAIN AND INTERFACE  #######################################################
substructuring = dss.twoSoilLayers()
interface_def_points = substructuring.interface_def_points
adjacent_subdomains = substructuring.adjacent_subdomains
subdomain_def_points = substructuring.subdomain_def_points
outer_boundary_def_points = substructuring.outer_boundary_def_points
~~~
This block sets the domain of simulation and its subdomain substructuring.
In this block you only need to change the line
~~~
substructuring = dss.twoSoilLayers()
~~~
to change the substructuring.
This block calls constructors of substructurings. These are classes defined in
`../LDDsimulation/domainSubstructuring.py`.
If you want to define your own substructurings have a look in that file.
Some explantion is given in that file on how to define substructuring classes.

### MODEL CONFIGURATION
The model configuration block sets all model parameters: Which model is assumed, material parameters, relative permeabilties, pc-S relationships etc.
Lets look at the first part:
~~~python
# MODEL CONFIGURATION #########################################################
isRichards = {
    1: True, #
    2: False
    }


viscosity = {#
# subdom_num : viscosity
    1: {'wetting' :1,
         'nonwetting': 1/50}, #
    2: {'wetting' :1,
         'nonwetting': 1/50}
}

porosity = {#
# subdom_num : porosity
    1: 0.22,#
    2: 0.22
}

# Dict of the form: { subdom_num : density }
densities = {
    1: {'wetting': 997,
        'nonwetting': 1.225},
    2: {'wetting': 997,
        'nonwetting': 1.225}
}

gravity_acceleration = 9.81

L = {#
# subdom_num : subdomain L for L-scheme
    1 : {'wetting' :Lw1,
         'nonwetting': Lnw1},#
    2 : {'wetting' :Lw2,
         'nonwetting': Lnw2}
}


lambda_param = {#
# interface_index : lambda parameter for the L-scheme
    0 : {'wetting' :lambda_w,
         'nonwetting': lambda_nw},#
}
~~~
All the dictionaries except for `lambda_param` use the subodomain index as
key. In `lambda_param` the index corresponds to the number of the interface.  These indices are defined by the substructuring sourced in `substructuring = dss.twoSoilLayers()` so that needs to fit. If there is a missmatch, the code
might through an error.
In detail:
- `isRichards`: toggle between the models. If set to `True` Richards model is
assumed, when set to `False`, the two-phase model.
- `viscosity`: Set the viscosity on each subdomain.
- `porosity`: Set the porosity on each subdomain.
- `densities`: Set the densities on each subdomain.
- `gravity_acceleration`: Set the gravity acceleration on each subdomain.
- `L`: Set the L parametrs on each subdomain. This gets filled with the information of `### LDD SCHEME PARAMETERS` block.
- `lambda_param`: Set the λ parameters for each interface. Again this gets filled with the information of `### LDD SCHEME PARAMETERS` block.

The relative permeabilities and pc-S relationships are set subsequently in
the block:

~~~python
intrinsic_permeability = {
    1: 0.01,
    2: 0.01,
}

# relative permeabilties
rel_perm_definition = {
    1: {"wetting": "Spow2",
        "nonwetting": "oneMinusSpow2"},
    2: {"wetting": "Spow3",
        "nonwetting": "oneMinusSpow3"},
}

rel_perm_dict = fts.generate_relative_permeability_dicts(rel_perm_definition)
relative_permeability = rel_perm_dict["ka"]
ka_prime = rel_perm_dict["ka_prime"]

# S-pc relation
Spc_on_subdomains = {
    1: {"testSpc": {"index": 1}},
    2: {"testSpc": {"index": 2}},
}

Spc = fts.generate_Spc_dicts(Spc_on_subdomains)
S_pc_sym = Spc["symbolic"]
S_pc_sym_prime = Spc["prime_symbolic"]
sat_pressure_relationship = Spc["dolfin"]
~~~

- `intrinsic_permeability`: each subdomain gets an intrinsic permeabilties. So far functions as intrinsic permeabilties have not been implemented. This is
because the numbers stored in the `intrinsic_permeability` dictionary get multiplied with the functions defined in the `rel_perm_definition` in the code.

- `rel_perm_definition`: The relative permeabilities get defined by calling a
a keyword specifying the type of function.
This gets then passed to the function
`fts.generate_relative_permeability_dicts` which generate the actual dictionaries needed by the simulation.
All this is defined in `../LDDsimulation/functions.py` and this has been done
to have a central way to defining and reusing data functions.
If you want to introduce other relative permeabilities, this is the place to do it.  
- `Spc_on_subdomains`: The Spc get defined similarly to the relative permeablities by calling a a keyword specifying the type of function along with a dictionary of parameters. As in the case of the relative permeabilities
the function `fts.generate_Spc_dicts(Spc_on_subdomains)` does the generation.
Again, `../LDDsimulation/functions.py` is the place to start hacking to get other parametrisations going.

>>>
**NOTE**
Even if you are using TPR couplings, the coupling conditions need nonwetting information even on the Richards domain, see my thesis.
Make sure to provide this information or the code will throw an error.
>>>

### MANUFACTURED SOLUTION/SOURCE EXPRESSION
~~~python
###############################################################################
# Manufacture source expressions with sympy #
###############################################################################
x, y = sym.symbols('x[0], x[1]')  # needed by UFL
t = sym.symbols('t', positive=True)

p_e_sym = {
    1: {'wetting': (-7.0 - (1.0 + t*t)*(1.0 + x*x + y*y))},
    2: {'wetting': (-7.0 - (1.0 + t*t)*(1.0 + x*x)),
        'nonwetting': (-2-t*(1.1+y + x**2))*y**2},
}

pc_e_sym = hlp.generate_exact_symbolic_pc(
                isRichards=isRichards,
                symbolic_pressure=p_e_sym
            )

symbols = {"x": x,
           "y": y,
           "t": t}
 # turn above symbolic code into exact solution for dolphin and
 # construct the rhs that matches the above exact solution.
 exact_solution_example = hlp.generate_exact_solution_expressions(
                         symbols=symbols,
                         isRichards=isRichards,
                         symbolic_pressure=p_e_sym,
                         symbolic_capillary_pressure=pc_e_sym,
                         saturation_pressure_relationship=S_pc_sym,
                         saturation_pressure_relationship_prime=S_pc_sym_prime,
                         viscosity=viscosity,
                         porosity=porosity,
                         intrinsic_permeability=intrinsic_permeability,
                         relative_permeability=relative_permeability,
                         relative_permeability_prime=ka_prime,
                         densities=densities,
                         gravity_acceleration=gravity_acceleration,
                         include_gravity=include_gravity,
                         )
 source_expression = exact_solution_example['source']
 exact_solution = exact_solution_example['exact_solution']
 initial_condition = exact_solution_example['initial_condition']

~~~
This block defines exact solution expressions and calculates source terms and
initial conditions from it.
If you dont want to have an exact solution, you can set
`exact_solution = None`.
Then you need to comment out the generation helper function specify
`source_expression` and `initial_condition` directly similarly to `p_e_sym`.
Have a look at the helper function `hlp.generate_exact_solution_expressions`
specified in `../LDDsimulation/helpers.py`.


### BOUNDARY CONDITIONS
~~~python
# BOUNDARY CONDITIONS #########################################################
# Dictionary of dirichlet boundary conditions. If an exact solution case is
# used, use the hlp.generate_exact_DirichletBC() method to generate the
# Dirichlet Boundary conditions from the exact solution.
dirichletBC = hlp.generate_exact_DirichletBC(
        isRichards=isRichards,
        outer_boundary_def_points=outer_boundary_def_points,
        exact_solution=exact_solution
    )
# If no exact solution is provided you need to provide a dictionary of boundary
# conditions. See the definiton of hlp.generate_exact_DirichletBC() to see
# the structure.
~~~
This block defines the boundary condition from the exact solution expression.
Again, if no exact solution is assumed, `dirichletBC` needs to be specified
manually, similar as explained above.

## LOG FILE OUTPUT
~~~python
# LOG FILE OUTPUT #############################################################
# read this file and print it to std out. This way the simulation can produce a
# log file with ./TP-R-layered_soil.py | tee simulation.log
f = open(thisfile, 'r')
print(f.read())
f.close()
~~~
This block does not need to be touched by the developer. It reads in the file
given in the `thisfile` variable, which is supposed to be the very simulation
file itself and prints it to `stdout`. When the simulation file is run at the
aid of one of the `run_simulation` script, this output gets piped into a logfile
for later reference. It is not beatiful, bit it works.

## MAIN
The main most likely will not have to be changed to run a new usecase unless
you want to change something in the parallelisation or add new parameters.

This function saves all the parameters into a dictionary `simulation_parameters`
and then passes that to the helper function `hlp.run_simulation(**kwrgs)`,
see `../LDDsimulation/helpers.py`, which in turn sets up the simulation object
and runs the simulation.
In case a mesh study is run the script `  hlp.merge_spacetime_errornorms(**kwrgs)`
is run to merge the space time error norms for each mesh size, that have been
saved in seperate files to allow parallel processing of the different mesh sizes.
~~~python
# MAIN ########################################################################
if __name__ == '__main__':
    # dictionary of simualation parameters to pass to the run function.
    # mesh_resolution and starttime are excluded, as they get passed explicitly
    # to achieve parallelisation in these parameters in these parameters for
    # mesh studies etc.
    simulation_parameter = {
        "tol": 1E-14,
        "debugflag": debugflag,
        "max_iter_num": max_iter_num,
        "FEM_Lagrange_degree": FEM_Lagrange_degree,
        "mesh_study": mesh_study,
        "use_case": use_case,
        "output_string": output_string,
        "subdomain_def_points": subdomain_def_points,
        "isRichards": isRichards,
        "interface_def_points": interface_def_points,
        "outer_boundary_def_points": outer_boundary_def_points,
        "adjacent_subdomains": adjacent_subdomains,
        # "mesh_resolution": mesh_resolution,
        "viscosity": viscosity,
        "porosity": porosity,
        "L": L,
        "lambda_param": lambda_param,
        "relative_permeability": relative_permeability,
        "intrinsic_permeability": intrinsic_permeability,
        "sat_pressure_relationship": sat_pressure_relationship,
        # "starttime": starttime,
        "number_of_timesteps": number_of_timesteps,
        "number_of_timesteps_to_analyse": number_of_timesteps_to_analyse,
        "plot_timestep_every": plot_timestep_every,
        "timestep_size": timestep_size,
        "source_expression": source_expression,
        "initial_condition": initial_condition,
        "dirichletBC": dirichletBC,
        "exact_solution": exact_solution,
        "densities": densities,
        "include_gravity": include_gravity,
        "gravity_acceleration": gravity_acceleration,
        "write_to_file": write_to_file,
        "analyse_condition": analyse_condition
    }
    for number_shift, starttime in starttimes.items():
        simulation_parameter.update(
            {"starttime_timestep_number_shift": number_shift}
        )
        for mesh_resolution, solver_tol in resolutions.items():
            simulation_parameter.update({"solver_tol": solver_tol})
            hlp.info(simulation_parameter["use_case"])
            processQueue = mp.Queue()
            LDDsim = mp.Process(
                        target=hlp.run_simulation,
                        args=(
                            simulation_parameter,
                            processQueue,
                            starttime,
                            mesh_resolution
                            )
                        )
            LDDsim.start()
            # LDDsim.join()
            # hlp.run_simulation(
            #     mesh_resolution=mesh_resolution,
            #     starttime=starttime,
            #     parameter=simulation_parameter
            #     )

        LDDsim.join()
        if mesh_study:
            simulation_output_dir = processQueue.get()
            hlp.merge_spacetime_errornorms(isRichards=isRichards,
                                           resolutions=resolutions,
                                           output_dir=simulation_output_dir)
~~~
