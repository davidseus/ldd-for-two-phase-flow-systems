#!/usr/bin/python3
import dolfin as df
import mshr
import numpy as np
import sympy as sym
import typing as tp
import domainPatch as dp
import LDDsimulation as ldd
import functools as ft
import helpers as hlp
import datetime
import os
import pandas as pd

date = datetime.datetime.now()
datestr = date.strftime("%Y-%m-%d")
#import ufl as ufl

# init sympy session
sym.init_printing()

use_case = "TP-R-2-patch-one-zero-source-only-pure-dd"
# solver_tol = 5E-7
max_iter_num = 1000
FEM_Lagrange_degree = 1
mesh_study = False
resolutions = {
                # 1: 1e-7,  # h=2
                # 2: 2e-5,  # h=1.1180
                # 4: 1e-6,  # h=0.5590
                # 8: 1e-6,  # h=0.2814
                # 16: 1e-6, # h=0.1412
                32: 1e-6,
                # 64: 5e-7,
                # 128: 5e-7
                }


############ GRID #######################
# mesh_resolution = 20
timestep_size = 0.001
number_of_timesteps = 20
plot_timestep_every = 1
# decide how many timesteps you want analysed. Analysed means, that we write out
# subsequent errors of the L-iteration within the timestep.
number_of_timesteps_to_analyse = 0
starttime = 0.0

Lw = 0.5 #/timestep_size
Lnw=Lw

lambda_w = 4
lambda_nw = 4

include_gravity = True
debugflag = True
analyse_condition = False

if mesh_study:
    output_string = "./output/{}-{}_timesteps{}_P{}".format(datestr, use_case, number_of_timesteps, FEM_Lagrange_degree)
else:
    for tol in resolutions.values():
        solver_tol = tol
    output_string = "./output/{}-{}_timesteps{}_P{}_solver_tol{}".format(datestr, use_case, number_of_timesteps, FEM_Lagrange_degree, solver_tol)


# toggle what should be written to files
if mesh_study:
    write_to_file = {
        'space_errornorms': True,
        'meshes_and_markers': True,
        'L_iterations_per_timestep': False,
        'solutions': False,
        'absolute_differences': False,
        'condition_numbers': analyse_condition,
        'subsequent_errors': False
    }
else:
    write_to_file = {
        'space_errornorms': True,
        'meshes_and_markers': True,
        'L_iterations_per_timestep': False,
        'solutions': True,
        'absolute_differences': True,
        'condition_numbers': analyse_condition,
        'subsequent_errors': True
    }

##### Domain and Interface ####
# global simulation domain domain
sub_domain0_vertices = [df.Point(-1.0,-1.0), #
                        df.Point(1.0,-1.0),#
                        df.Point(1.0,1.0),#
                        df.Point(-1.0,1.0)]
# interface between subdomain1 and subdomain2
interface12_vertices = [df.Point(-1.0, 0.9),
                        df.Point(1.0, 0.4) ]
                        # interface equation: y = -1/4*x + 13/20
# subdomain1.
sub_domain1_vertices = [interface12_vertices[0],
                        interface12_vertices[1],
                        sub_domain0_vertices[2],
                        sub_domain0_vertices[3] ]

# vertex coordinates of the outer boundaries. If it can not be specified as a
# polygon, use an entry per boundary polygon. This information is used for defining
# the Dirichlet boundary conditions. If a domain is completely internal, the
# dictionary entry should be 0: None
subdomain1_outer_boundary_verts = {
    0: [interface12_vertices[1],
        sub_domain0_vertices[2],
        sub_domain0_vertices[3], #
        interface12_vertices[0]]
}
# subdomain2
sub_domain2_vertices = [sub_domain0_vertices[0],
                        sub_domain0_vertices[1],
                        interface12_vertices[1],
                        interface12_vertices[0] ]

subdomain2_outer_boundary_verts = {
    0: [interface12_vertices[0], #
        sub_domain0_vertices[0],
        sub_domain0_vertices[1],
        interface12_vertices[1]]
}

# list of subdomains given by the boundary polygon vertices.
# Subdomains are given as a list of dolfin points forming
# a closed polygon, such that mshr.Polygon(subdomain_def_points[i]) can be used
# to create the subdomain. subdomain_def_points[0] contains the
# vertices of the global simulation domain and subdomain_def_points[i] contains the
# vertices of the subdomain i.
subdomain_def_points = [sub_domain0_vertices,#
                      sub_domain1_vertices,#
                      sub_domain2_vertices]
# in the below list, index 0 corresponds to the 12 interface which has index 1
interface_def_points = [interface12_vertices]

# if a subdomain has no outer boundary write None instead, i.e.
# i: None
# if i is the index of the inner subdomain.
outer_boundary_def_points = {
    # subdomain number
    1 : subdomain1_outer_boundary_verts,
    2 : subdomain2_outer_boundary_verts
}

# adjacent_subdomains[i] contains the indices of the subdomains sharing the
# interface i (i.e. given by interface_def_points[i]).
adjacent_subdomains = [[1,2]]
isRichards = {
    1: True, #
    2: False
    }


viscosity = {#
# subdom_num : viscosity
    1 : {'wetting' :1,
         'nonwetting': 1}, #
    2 : {'wetting' :1,
         'nonwetting': 1}
}

porosity = {#
# subdom_num : porosity
    1 : 1,#
    2 : 1
}

# Dict of the form: { subdom_num : density }
densities = {
    1: {'wetting': 1,  #997,
        'nonwetting': 1}, #1225},
    2: {'wetting': 1,  #997,
        'nonwetting': 1}, #1225},
}

intrinsic_permeability = {
    1: {"wetting": 1,
        "nonwetting": 1},
    2: {"wetting": 1,
        "nonwetting": 1},
}


gravity_acceleration = 1

L = {#
# subdom_num : subdomain L for L-scheme
    1 : {'wetting' :Lw,
         'nonwetting': Lnw},#
    2 : {'wetting' :Lw,
         'nonwetting': Lnw}
}


lambda_param = {#
# subdom_num : lambda parameter for the L-scheme
    1 : {'wetting' :lambda_w,
         'nonwetting': lambda_nw},#
    2 : {'wetting' :lambda_w,
         'nonwetting': lambda_nw}
}

## relative permeabilty functions on subdomain 1
def rel_perm1w(s):
    # relative permeabilty wetting on subdomain1
    return intrinsic_permeability[1]["wetting"]*s**2

def rel_perm1nw(s):
    # relative permeabilty nonwetting on subdomain1
    return intrinsic_permeability[1]["nonwetting"]*(1-s)**2

_rel_perm1w = ft.partial(rel_perm1w)
_rel_perm1nw = ft.partial(rel_perm1nw)

subdomain1_rel_perm = {
    'wetting': _rel_perm1w,#
    'nonwetting': _rel_perm1nw
}
## relative permeabilty functions on subdomain 2
def rel_perm2w(s):
    # relative permeabilty wetting on subdomain2
    return intrinsic_permeability[2]["wetting"]*s**2
def rel_perm2nw(s):
    # relative permeabilty nonwetting on subdosym.cos(0.8*t - (0.8*x + 1/7*y))main2
    return intrinsic_permeability[2]["nonwetting"]*(1-s)**2

_rel_perm2w = ft.partial(rel_perm2w)
_rel_perm2nw = ft.partial(rel_perm2nw)

# subdomain2_rel_perm = {
#     'wetting': _rel_perm2w,#
#     'nonwetting': _rel_perm2nw
# }
#
subdomain2_rel_perm = {
    'wetting': _rel_perm1w,#
    'nonwetting': _rel_perm1nw
}

## dictionary of relative permeabilties on all domains.
relative_permeability = {#
    1: subdomain1_rel_perm,
    2: subdomain2_rel_perm
}


# definition of the derivatives of the relative permeabilities
# relative permeabilty functions on subdomain 1
def rel_perm1w_prime(s):
    # relative permeabilty on subdomain1
    return intrinsic_permeability[1]["wetting"]*2*s

def rel_perm1nw_prime(s):
    # relative permeabilty on subdomain1
    return -1*intrinsic_permeability[1]["nonwetting"]*2*(1-s)

# # definition of the derivatives of the relative permeabilities
# # relative permeabilty functions on subdomain 1
def rel_perm2w_prime(s):
    # relative permeabilty on subdomain1
    return intrinsic_permeability[2]["wetting"]*2*s

def rel_perm2nw_prime(s):
    # relative permeabilty on subdomain1
    return -1*intrinsic_permeability[2]["nonwetting"]*2*(1-s)

_rel_perm1w_prime = ft.partial(rel_perm1w_prime)
_rel_perm1nw_prime = ft.partial(rel_perm1nw_prime)
_rel_perm2w_prime = ft.partial(rel_perm2w_prime)
_rel_perm2nw_prime = ft.partial(rel_perm2nw_prime)

subdomain1_rel_perm_prime = {
    'wetting': _rel_perm1w_prime,
    'nonwetting': _rel_perm1nw_prime
}


# subdomain2_rel_perm_prime = {
#     'wetting': _rel_perm2w_prime,
#     'nonwetting': _rel_perm2nw_prime
# }

subdomain2_rel_perm_prime = {
    'wetting': _rel_perm1w_prime,
    'nonwetting': _rel_perm1nw_prime
}

# dictionary of relative permeabilties on all domains.
ka_prime = {
    1: subdomain1_rel_perm_prime,
    2: subdomain2_rel_perm_prime,
}



def saturation(pc, index):
    # inverse capillary pressure-saturation-relationship
    return df.conditional(pc > 0, 1/((1 + pc)**(1/(index + 1))), 1)


def saturation_sym(pc, index):
    # inverse capillary pressure-saturation-relationship
    return 1/((1 + pc)**(1/(index + 1)))


# derivative of S-pc relationship with respect to pc. This is needed for the
# construction of a analytic solution.
def saturation_sym_prime(pc, index):
    # inverse capillary pressure-saturation-relationship
    return -1/((index+1)*(1 + pc)**((index+2)/(index+1)))


# note that the conditional definition of S-pc in the nonsymbolic part will be
# incorporated in the construction of the exact solution below.
S_pc_sym = {
    1: ft.partial(saturation_sym, index=2),
    2: ft.partial(saturation_sym, index=2),
    # 3: ft.partial(saturation_sym, index=2),
    # 4: ft.partial(saturation_sym, index=1)
}

S_pc_sym_prime = {
    1: ft.partial(saturation_sym_prime, index=2),
    2: ft.partial(saturation_sym_prime, index=2),
    # 3: ft.partial(saturation_sym_prime, index=2),
    # 4: ft.partial(saturation_sym_prime, index=1)
}

sat_pressure_relationship = {
    1: ft.partial(saturation, index=2),
    2: ft.partial(saturation, index=2),
    # 3: ft.partial(saturation, index=2),
    # 4: ft.partial(saturation, index=1)
}

#
# def saturation(pc, n_index, alpha):
#     # inverse capillary pressure-saturation-relationship
#     return df.conditional(pc > 0, 1/((1 + (alpha*pc)**n_index)**((n_index - 1)/n_index)), 1)
#
# # S-pc-relation ship. We use the van Genuchten approach, i.e. pc = 1/alpha*(S^{-1/m} -1)^1/n, where
# # we set alpha = 0, assume m = 1-1/n (see Helmig) and assume that residual saturation is Sw
# def saturation_sym(pc, n_index, alpha):
#     # inverse capillary pressure-saturation-relationship
#     #df.conditional(pc > 0,
#     return 1/((1 + (alpha*pc)**n_index)**((n_index - 1)/n_index))
#
#
# # derivative of S-pc relationship with respect to pc. This is needed for the
# # construction of a analytic solution.
# def saturation_sym_prime(pc, n_index, alpha):
#     # inverse capillary pressure-saturation-relationship
#     return -(alpha*(n_index - 1)*(alpha*pc)**(n_index - 1)) / ( (1 + (alpha*pc)**n_index)**((2*n_index - 1)/n_index) )
#
# # note that the conditional definition of S-pc in the nonsymbolic part will be
# # incorporated in the construction of the exact solution below.
# S_pc_sym = {
#     1: ft.partial(saturation_sym, n_index=3, alpha=0.001),
#     2: ft.partial(saturation_sym, n_index=6, alpha=0.001),
#     # 3: ft.partial(saturation_sym, n_index=3, alpha=0.001),
#     # 4: ft.partial(saturation_sym, n_index=3, alpha=0.001),
#     # 5: ft.partial(saturation_sym, n_index=3, alpha=0.001),
#     # 6: ft.partial(saturation_sym, n_index=3, alpha=0.001)
# }
#
# S_pc_sym_prime = {
#     1: ft.partial(saturation_sym_prime, n_index=3, alpha=0.001),
#     2: ft.partial(saturation_sym_prime, n_index=6, alpha=0.001),
#     # 3: ft.partial(saturation_sym_prime, n_index=3, alpha=0.001),
#     # 4: ft.partial(saturation_sym_prime, n_index=3, alpha=0.001),
#     # 5: ft.partial(saturation_sym_prime, n_index=3, alpha=0.001),
#     # 6: ft.partial(saturation_sym_prime, n_index=3, alpha=0.001)
# }
#
# sat_pressure_relationship = {
#     1: ft.partial(saturation, n_index=3, alpha=0.001),
#     2: ft.partial(saturation, n_index=6, alpha=0.001),
#     # 3: ft.partial(saturation, n_index=3, alpha=0.001),
#     # 4: ft.partial(saturation, n_index=3, alpha=0.001),
#     # 5: ft.partial(saturation, n_index=3, alpha=0.001),
#     # 6: ft.partial(saturation, n_index=3, alpha=0.001)
# }
#


#############################################
# Manufacture source expressions with sympy #
#############################################
x, y = sym.symbols('x[0], x[1]')  # needed by UFL
t = sym.symbols('t', positive=True)

initial_condition = {
    1: {'wetting': sym.printing.ccode(-6 - (1+t*t)*(1 + x*x + y*y)),  #*cutoff,
        'nonwetting': sym.printing.ccode(-1 -t*(1.1+ y*y))},  #*(sym.sin((1+y)/2*sym.pi)*sym.sin((1+x)/2*sym.pi))**2},
    2: {'wetting': sym.printing.ccode(-6 - (1+t*t)*(1 + x*x + y*y)),  #*(sym.sin((1+y)/2*sym.pi)*sym.sin((1+x)/2*sym.pi))**2,
        'nonwetting': sym.printing.ccode(-1 -t*(1.1 + y*y))},  #*(sym.sin((1+y)/2*sym.pi)*sym.sin((1+x)/2*sym.pi))**2},
}

initial_condition = {
    1: {'wetting': sym.printing.ccode(-6*(1-x*x)*(1-y*y)),  #*cutoff,
        'nonwetting': sym.printing.ccode(-(1-x*x)*(1-y*y))},  #*(sym.sin((1+y)/2*sym.pi)*sym.sin((1+x)/2*sym.pi))**2},
    2: {'wetting': sym.printing.ccode(-6*(1-x*x)*(1-y*y)),  #*(sym.sin((1+y)/2*sym.pi)*sym.sin((1+x)/2*sym.pi))**2,
        'nonwetting': sym.printing.ccode(-(1-x*x)*(1-y*y))},  #*(sym.sin((1+y)/2*sym.pi)*sym.sin((1+x)/2*sym.pi))**2},
}

### constructing source experessions.
injection_coord = [-0.75, -0.7]
extraction_coord = [0.75, 0.7]
injection_radius = 0.1
extraction_radius = 0.1
# epsilon_y_inner = epsilon_x_inner
# epsilon_y_outer = epsilon_x_outer
#
# def mollifier(x, epsilon):
#     """ one d mollifier """
#     out_expr = sym.exp(-1/(1-(x/epsilon)**2) + 1)
#     return out_expr
#
# mollifier_handle = ft.partial(mollifier, epsilon=epsilon_x_inner)
#
# pw_sym_x = sym.Piecewise(
#     (mollifier_handle(x), x**2 < epsilon_x_outer**2),
#     (0, True)
# )
# pw_sym_y = sym.Piecewise(
#     (mollifier_handle(y), y**2 < epsilon_y_outer**2),
#     (0, True)
# )
#
def mollifier2d(x, y, epsilon):
    """ one d mollifier """
    out_expr = 0.05*sym.exp(-1/(1-(x**2 + y**2)/epsilon**2))
    return out_expr

mollifier2d_handle_i = ft.partial(mollifier2d, epsilon=injection_radius)

source_in = sym.Piecewise(
    (-(1/(1 + t**2))*mollifier2d_handle_i(x, y), (x-injection_coord[0])**2 + (y-injection_coord[1])**2 < injection_radius**2),
    (0*t, True)
)

mollifier2d_handle_e = ft.partial(mollifier2d, epsilon=extraction_radius)

source_ext = sym.Piecewise(
    (-(1/(1 + t**2))*mollifier2d_handle_e(x, y), (x-extraction_coord[0])**2 + (y-extraction_coord[1])**2 < extraction_radius**2),
    (0*t, True)
)

extraction_water_ratio = 0.3
injection_water_ratio = 0.3

# {"wetting": sym.printing.ccode(extraction_water_ratio*source_ext),
#     "nonwetting": sym.printing.ccode((1-extraction_water_ratio)*source_ext)}
source_expression = {
    1: {"wetting": sym.printing.ccode(0*t),
        "nonwetting": sym.printing.ccode(0*t)},
    2: {"wetting": sym.printing.ccode(0*t), #sym.printing.ccode(extraction_water_ratio*source_ext),
        "nonwetting": sym.printing.ccode(0*t)} #sym.printing.ccode((1-extraction_water_ratio)*source_ext)}
}

exact_solution = None
#
# zero_on_epsilon_shrinking_of_subdomain = sym.Piecewise(
#     (mollifier_handle(sym.sqrt(x**2 + y**2)+2*epsilon_x_inner), ((-2*epsilon_x_inner<sym.sqrt(x**2 + y**2)) & (sym.sqrt(x**2 + y**2)<-epsilon_x_inner))),
#     (0, ((-epsilon_x_inner<=sym.sqrt(x**2 + y**2)) & (sym.sqrt(x**2 + y**2)<=epsilon_x_inner))),
#     (mollifier_handle(sym.sqrt(x**2 + y**2)-2*epsilon_x_inner), ((epsilon_x_inner<sym.sqrt(x**2 + y**2)) & (sym.sqrt(x**2 + y**2)<2*epsilon_x_inner))),
#     (1, True),
# )
#
# zero_on_epsilon_shrinking_of_subdomain_x = sym.Piecewise(
#     (mollifier_handle(x+2*epsilon_x_inner), ((-2*epsilon_x_inner<x) & (x<-epsilon_x_inner))),
#     (0, ((-epsilon_x_inner<=x) & (x<=epsilon_x_inner))),
#     (mollifier_handle(x-2*epsilon_x_inner), ((epsilon_x_inner<x) & (x<2*epsilon_x_inner))),
#     (1, True),
# )
#
# zero_on_epsilon_shrinking_of_subdomain_y = sym.Piecewise(
#     (1, y<=-2*epsilon_x_inner),
#     (mollifier_handle(y+2*epsilon_x_inner), ((-2*epsilon_x_inner<y) & (y<-epsilon_x_inner))),
#     (0, ((-epsilon_x_inner<=y) & (y<=epsilon_x_inner))),
#     (mollifier_handle(y-2*epsilon_x_inner), ((epsilon_x_inner<y) & (y<2*epsilon_x_inner))),
#     (1, True),
# )
#
# zero_on_shrinking = zero_on_epsilon_shrinking_of_subdomain #zero_on_epsilon_shrinking_of_subdomain_x + zero_on_epsilon_shrinking_of_subdomain_y
# gaussian = pw_sym2d_x# pw_sym_y*pw_sym_x
# cutoff = gaussian/(gaussian + zero_on_shrinking)


# pc_e_sym = dict()
# for subdomain, isR in isRichards.items():
#     if isR:
#         pc_e_sym.update({subdomain: -p_e_sym[subdomain]['wetting'].copy()})
#     else:
#         pc_e_sym.update({subdomain: p_e_sym[subdomain]['nonwetting'].copy()
#                                         - p_e_sym[subdomain]['wetting'].copy()})


symbols = {"x": x,
           "y": y,
           "t": t}
# # turn above symbolic code into exact solution for dolphin and
# # construct the rhs that matches the above exact solution.
# exact_solution_example = hlp.generate_exact_solution_expressions(
#                         symbols=symbols,
#                         isRichards=isRichards,
#                         symbolic_pressure=p_e_sym,
#                         symbolic_capillary_pressure=pc_e_sym,
#                         saturation_pressure_relationship=S_pc_sym,
#                         saturation_pressure_relationship_prime=S_pc_sym_prime,
#                         viscosity=viscosity,
#                         porosity=porosity,
#                         relative_permeability=relative_permeability,
#                         relative_permeability_prime=ka_prime,
#                         densities=densities,
#                         gravity_acceleration=gravity_acceleration,
#                         include_gravity=include_gravity,
#                         )
# source_expression = exact_solution_example['source']
# exact_solution = exact_solution_example['exact_solution']
# initial_condition = exact_solution_example['initial_condition']

# Dictionary of dirichlet boundary conditions.
dirichletBC = dict()
# similarly to the outer boundary dictionary, if a patch has no outer boundary
# None should be written instead of an expression.
# This is a bit of a brainfuck:
# dirichletBC[ind] gives a dictionary of the outer boundaries of subdomain ind.
# Since a domain patch can have several disjoint outer boundary parts, the
# expressions need to get an enumaration index which starts at 0.
# So dirichletBC[ind][j] is the dictionary of outer dirichlet conditions of
# subdomain ind and boundary part j.
# Finally, dirichletBC[ind][j]['wetting'] and dirichletBC[ind][j]['nonwetting']
# return the actual expression needed for the dirichlet condition for both
# phases if present.

# subdomain index: {outer boudary part index: {phase: expression}}
for subdomain in isRichards.keys():
    # if subdomain has no outer boundary, outer_boundary_def_points[subdomain] is None
    if outer_boundary_def_points[subdomain] is None:
        dirichletBC.update({subdomain: None})
    else:
        dirichletBC.update({subdomain: dict()})
        # set the dirichlet conditions to be the same code as exact solution on
        # the subdomain.
        for outer_boundary_ind in outer_boundary_def_points[subdomain].keys():
            dirichletBC[subdomain].update(
                # {outer_boundary_ind: exact_solution[subdomain]}
                {
                    outer_boundary_ind: {
                        "wetting": sym.printing.ccode(0*t),
                        "nonwetting": sym.printing.ccode(0*t)
                        }
                }
                )


# def saturation(pressure, subdomain_index):
#     # inverse capillary pressure-saturation-relationship
#     return df.conditional(pressure < 0, 1/((1 - pressure)**(1/(subdomain_index + 1))), 1)
#
# sa

for mesh_resolution, solver_tol in resolutions.items():
    # initialise LDD simulation class
    simulation = ldd.LDDsimulation(
        tol=1E-14,
        LDDsolver_tol=solver_tol,
        debug=debugflag,
        max_iter_num=max_iter_num,
        FEM_Lagrange_degree=FEM_Lagrange_degree,
        mesh_study=mesh_study
        )

    simulation.set_parameters(use_case=use_case,
                              output_dir=output_string,
                              subdomain_def_points=subdomain_def_points,
                              isRichards=isRichards,
                              interface_def_points=interface_def_points,
                              outer_boundary_def_points=outer_boundary_def_points,
                              adjacent_subdomains=adjacent_subdomains,
                              mesh_resolution=mesh_resolution,
                              viscosity=viscosity,
                              porosity=porosity,
                              L=L,
                              lambda_param=lambda_param,
                              relative_permeability=relative_permeability,
                              saturation=sat_pressure_relationship,
                              starttime=starttime,
                              number_of_timesteps=number_of_timesteps,
                              number_of_timesteps_to_analyse=number_of_timesteps_to_analyse,
                              plot_timestep_every=plot_timestep_every,
                              timestep_size=timestep_size,
                              sources=source_expression,
                              initial_conditions=initial_condition,
                              dirichletBC_expression_strings=dirichletBC,
                              exact_solution=exact_solution,
                              densities=densities,
                              include_gravity=include_gravity,
                              write2file=write_to_file,
                              )

    simulation.initialise()
    output_dir = simulation.output_dir
    # simulation.write_exact_solution_to_xdmf()
    output = simulation.run(analyse_condition=analyse_condition)
    for subdomain_index, subdomain_output in output.items():
        mesh_h = subdomain_output['mesh_size']
        for phase, different_errornorms in subdomain_output['errornorm'].items():
            filename = output_dir + "subdomain{}-space-time-errornorm-{}-phase.csv".format(subdomain_index, phase)
            # for errortype, errornorm in different_errornorms.items():

                # eocfile = open("eoc_filename", "a")
                # eocfile.write( str(mesh_h) + " " + str(errornorm) + "\n" )
                # eocfile.close()
                # if subdomain.isRichards:mesh_h
            data_dict = {
                'mesh_parameter': mesh_resolution,
                'mesh_h': mesh_h,
            }
            for error_type, errornorms in different_errornorms.items():
                data_dict.update(
                    {error_type: errornorms}
                )
            errors = pd.DataFrame(data_dict, index=[mesh_resolution])
            # check if file exists
            if os.path.isfile(filename) == True:
                with open(filename, 'a') as f:
                    errors.to_csv(f, header=False, sep='\t', encoding='utf-8', index=False)
            else:
                errors.to_csv(filename, sep='\t', encoding='utf-8', index=False)
