# global domain
subdomain0_vertices = [df.Point(0.0,0.0), #
                        df.Point(13.0,0.0),#
                        df.Point(13.0,8.0),#
                        df.Point(0.0,8.0)]

interface12_vertices = [df.Point(0.0, 7.0),
                        df.Point(9.0, 7.0),
                        df.Point(10.5, 7.5),
                        df.Point(12.0, 7.0),
                        df.Point(13.0, 6.5)]
# subdomain1.
subdomain1_vertices = [interface12_vertices[0],
                        interface12_vertices[1],
                        interface12_vertices[2],
                        interface12_vertices[3],
                        interface12_vertices[4], # southern boundary, 12 interface
                        subdomain0_vertices[2], # eastern boundary, outer boundary
                        subdomain0_vertices[3]] # northern boundary, outer on_boundary

# vertex coordinates of the outer boundaries. If it can not be specified as a
# polygon, use an entry per boundary polygon. This information is used for defining
# the Dirichlet boundary conditions. If a domain is completely internal, the
# dictionary entry should be 0: None
subdomain1_outer_boundary_verts = {
    0: [interface12_vertices[4], #
        subdomain0_vertices[2], # eastern boundary, outer boundary
        subdomain0_vertices[3],
        interface12_vertices[0]]
}


# interface23
interface23_vertices = [df.Point(0.0, 5.0),
                        df.Point(3.0, 5.0),
                        # df.Point(6.5, 4.5),
                        df.Point(6.5, 5.0),
                        df.Point(9.5, 5.0),
                        # df.Point(11.5, 3.5),
                        # df.Point(13.0, 3)
                        df.Point(11.5, 5.0),
                        df.Point(13.0, 5.0)
                        ]

#subdomain1
subdomain2_vertices = [interface23_vertices[0],
                        interface23_vertices[1],
                        interface23_vertices[2],
                        interface23_vertices[3],
                        interface23_vertices[4],
                        interface23_vertices[5], # southern boundary, 23 interface
                        subdomain1_vertices[4], # eastern boundary, outer boundary
                        subdomain1_vertices[3],
                        subdomain1_vertices[2],
                        subdomain1_vertices[1],
                        subdomain1_vertices[0] ] # northern boundary, 12 interface

subdomain2_outer_boundary_verts = {
    0: [interface23_vertices[5],
        subdomain1_vertices[4]],
    1: [subdomain1_vertices[0],
        interface23_vertices[0]]
}


# interface34
interface34_vertices = [df.Point(0.0, 2.0),
                        df.Point(4.0, 2.0),
                        df.Point(9.0, 2.5),
                        df.Point(10.5, 2.0),
                        df.Point(13.0, 1.5)]

# subdomain3
subdomain3_vertices = [interface34_vertices[0],
                        interface34_vertices[1],
                        interface34_vertices[2],
                        interface34_vertices[3],
                        interface34_vertices[4], # southern boundary, 34 interface
                        subdomain2_vertices[5], # eastern boundary, outer boundary
                        subdomain2_vertices[4],
                        subdomain2_vertices[3],
                        subdomain2_vertices[2],
                        subdomain2_vertices[1],
                        subdomain2_vertices[0] ] # northern boundary, 23 interface

subdomain3_outer_boundary_verts = {
    0: [interface34_vertices[4],
        subdomain2_vertices[5]],
    1: [subdomain2_vertices[0],
        interface34_vertices[0]]
}

# subdomain4
subdomain4_vertices = [subdomain0_vertices[0],
                        subdomain0_vertices[1], # southern boundary, outer boundary
                        subdomain3_vertices[4],# eastern boundary, outer boundary
                        subdomain3_vertices[3],
                        subdomain3_vertices[2],
                        subdomain3_vertices[1],
                        subdomain3_vertices[0] ] # northern boundary, 34 interface

subdomain4_outer_boundary_verts = {
    0: [subdomain4_vertices[6],
        subdomain4_vertices[0],
        subdomain4_vertices[1],
        subdomain4_vertices[2]]
}


subdomain_def_points = [subdomain0_vertices,#
                      subdomain1_vertices,#
                      subdomain2_vertices,#
                      subdomain3_vertices,#
                      subdomain4_vertices
                      ]
