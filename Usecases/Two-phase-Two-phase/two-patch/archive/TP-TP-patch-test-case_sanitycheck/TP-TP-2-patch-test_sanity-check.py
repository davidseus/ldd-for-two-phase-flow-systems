#!/usr/bin/python3
import dolfin as df
import mshr
import numpy as np
import sympy as sym
import typing as tp
import domainPatch as dp
import LDDsimulation as ldd
import functools as ft
#import ufl as ufl

# init sympy session
sym.init_printing()

##### Domain and Interface ####
# global simulation domain domain
sub_domain0_vertices = [df.Point(0.0,0.0), #
                        df.Point(1.0,0.0),#
                        df.Point(1.0,1.0),#
                        df.Point(0.0,1.0)]
# interface between subdomain1 and subdomain2
interface12_vertices = [df.Point(0.0, 0.5),
                        df.Point(1.0, 0.5) ]
# subdomain1.
sub_domain1_vertices = [interface12_vertices[0],
                        interface12_vertices[1],
                        df.Point(1.0,1.0),
                        df.Point(0.0,1.0) ]

# vertex coordinates of the outer boundaries. If it can not be specified as a
# polygon, use an entry per boundary polygon. This information is used for defining
# the Dirichlet boundary conditions. If a domain is completely internal, the
# dictionary entry should be 0: None
subdomain1_outer_boundary_verts = {
    0: [interface12_vertices[0], #
        df.Point(0.0,1.0), #
        df.Point(1.0,1.0), #
        interface12_vertices[1]]
}
# subdomain2
sub_domain2_vertices = [df.Point(0.0,0.0),
                        df.Point(1.0,0.0),
                        interface12_vertices[1],
                        interface12_vertices[0] ]

subdomain2_outer_boundary_verts = {
    0: [interface12_vertices[1], #
        df.Point(1.0,0.0), #
        df.Point(0.0,0.0), #
        interface12_vertices[0]]
}
# subdomain2_outer_boundary_verts = {
#     0: [interface12_vertices[0], df.Point(0.0,0.0)],#
#     1: [df.Point(0.0,0.0), df.Point(1.0,0.0)], #
#     2: [df.Point(1.0,0.0), interface12_vertices[1]]
# }
# subdomain2_outer_boundary_verts = {
#     0: None
# }

# list of subdomains given by the boundary polygon vertices.
# Subdomains are given as a list of dolfin points forming
# a closed polygon, such that mshr.Polygon(subdomain_def_points[i]) can be used
# to create the subdomain. subdomain_def_points[0] contains the
# vertices of the global simulation domain and subdomain_def_points[i] contains the
# vertices of the subdomain i.
subdomain_def_points = [sub_domain0_vertices,#
                      sub_domain1_vertices,#
                      sub_domain2_vertices]
# in the below list, index 0 corresponds to the 12 interface which has index 1
interface_def_points = [interface12_vertices]

# if a subdomain has no outer boundary write None instead, i.e.
# i: None
# if i is the index of the inner subdomain.
outer_boundary_def_points = {
    # subdomain number
    1 : subdomain1_outer_boundary_verts,
    2 : subdomain2_outer_boundary_verts
}

# adjacent_subdomains[i] contains the indices of the subdomains sharing the
# interface i (i.e. given by interface_def_points[i]).
adjacent_subdomains = [[1,2]]
isRichards = {
    1: False, #
    2: False
    }


############ GRID ########################ü
mesh_resolution = 20
timestep_size = 1*0.001
number_of_timesteps = 20
# decide how many timesteps you want analysed. Analysed means, that we write out
# subsequent errors of the L-iteration within the timestep.
number_of_timesteps_to_analyse = 11
starttime = 0

viscosity = {#
# subdom_num : viscosity
    1 : {'wetting' :1,
         'nonwetting': 1/50}, #
    2 : {'wetting' :1,
         'nonwetting': 1/50}
}

porosity = {#
# subdom_num : porosity
    1 : 1,#
    2 : 1
}

L = {#
# subdom_num : subdomain L for L-scheme
    1 : {'wetting' :0.25,
         'nonwetting': 0.25},#
    2 : {'wetting' :0.25,
         'nonwetting': 0.25}
}

lambda_param = {#
# subdom_num : lambda parameter for the L-scheme
    1 : {'wetting' :140,
         'nonwetting': 2400},#
    2 : {'wetting' :140,
         'nonwetting': 2400}
}

## relative permeabilty functions on subdomain 1
def rel_perm1w(s):
    # relative permeabilty wetting on subdomain1
    return s**2

def rel_perm1nw(s):
    # relative permeabilty nonwetting on subdomain1
    return (1-s)**2

_rel_perm1w = ft.partial(rel_perm1w)
_rel_perm1nw = ft.partial(rel_perm1nw)
subdomain1_rel_perm = {
    'wetting': _rel_perm1w,#
    'nonwetting': _rel_perm1nw
}
## relative permeabilty functions on subdomain 2
def rel_perm2w(s):
    # relative permeabilty wetting on subdomain2
    return s**2
def rel_perm2nw(s):
    # relative permeabilty nonwetting on subdomain2
    return (1-s)**2

_rel_perm2w = ft.partial(rel_perm2w)
_rel_perm2nw = ft.partial(rel_perm2nw)

subdomain2_rel_perm = {
    'wetting': _rel_perm2w,#
    'nonwetting': _rel_perm2nw
}

## dictionary of relative permeabilties on all domains.
relative_permeability = {#
    1: subdomain1_rel_perm,
    2: subdomain2_rel_perm
}

# S-pc-relation ship. We use the van Genuchten approach, i.e. pc = 1/alpha*(S^{-1/m} -1)^1/n, where
# we set alpha = 0, assume m = 1-1/n (see Helmig) and assume that residual saturation is Sw
def saturation(capillary_pressure, n_index, alpha):
    # inverse capillary pressure-saturation-relationship
    return df.conditional(capillary_pressure > 0, 1/((1 + (alpha*capillary_pressure)**n_index)**((n_index - 1)/n_index)), 1)

# S-pc-relation ship. We use the van Genuchten approach, i.e. pc = 1/alpha*(S^{-1/m} -1)^1/n, where
# we set alpha = 0, assume m = 1-1/n (see Helmig) and assume that residual saturation is Sw
def saturation_sym(capillary_pressure, n_index, alpha):
    # inverse capillary pressure-saturation-relationship
    #df.conditional(capillary_pressure > 0,
    return 1/((1 + (alpha*capillary_pressure)**n_index)**((n_index - 1)/n_index))

S_pc_rel = {#
    1: ft.partial(saturation_sym, n_index = 3, alpha=0.001),# n= 3 stands for non-uniform porous media
    2: ft.partial(saturation_sym, n_index = 3, alpha=0.001) # n=6 stands for uniform porous media matrix (siehe Helmig)
}

S_pc_rel_sym = {#
    1: ft.partial(saturation_sym, n_index = sym.Symbol('n'), alpha = sym.Symbol('a')),# n= 3 stands for non-uniform porous media
    2: ft.partial(saturation_sym, n_index = sym.Symbol('n'), alpha = sym.Symbol('a')) # n=6 stands for uniform porous media matrix (siehe Helmig)
}

#### Manufacture source expressions with sympy
###############################################################################
## subdomain1
x, y = sym.symbols('x[0], x[1]') # needed by UFL
t = sym.symbols('t', positive=True)
#f = -sym.diff(u, x, 2) - sym.diff(u, y, 2)  # -Laplace(u)
#f = sym.simplify(f)                         # simplify f
p1_w = -20 - (1+t*t)*(1 + x**2 + (y-0.5)**2)
p1_nw = -t*(1-(y-0.5) + x**2)**2 - sym.sqrt(2+t**2)*(1 + (y-0.5)**2 + x**2)


#dtS1_w = sym.diff(S_pc_rel_sym[1](p1_nw - p1_w), t, 1)
#dtS1_nw = -sym.diff(S_pc_rel_sym[1](p1_nw - p1_w), t, 1)
dtS1_w = porosity[1]*sym.diff(S_pc_rel[1](p1_nw - p1_w), t, 1)
dtS1_nw = -porosity[1]*sym.diff(S_pc_rel[1](p1_nw - p1_w), t, 1)
print("dtS1_w = ", dtS1_w, "\n")
print("dtS1_nw = ", dtS1_nw, "\n")

#dxdxflux1_w = -sym.diff(relative_permeability[1]['wetting'](S_pc_rel_sym[1](p1_nw - p1_w))*sym.diff(p1_w, x, 1), x, 1)
#dydyflux1_w = -sym.diff(relative_permeability[1]['wetting'](S_pc_rel_sym[1](p1_nw - p1_w))*sym.diff(p1_w, y, 1), y, 1)
dxdxflux1_w = -1/viscosity[1]['wetting']*sym.diff(relative_permeability[1]['wetting'](S_pc_rel[1](p1_nw - p1_w))*sym.diff(p1_w, x, 1), x, 1)
dydyflux1_w = -1/viscosity[1]['wetting']*sym.diff(relative_permeability[1]['wetting'](S_pc_rel[1](p1_nw - p1_w))*sym.diff(p1_w, y, 1), y, 1)

rhs1_w = dtS1_w + dxdxflux1_w + dydyflux1_w
rhs1_w = sym.printing.ccode(rhs1_w)
print("rhs_w = ", rhs1_w, "\n")
#rhs_w = sym.expand(rhs_w)
#print("rhs_w", rhs_w, "\n")
#rhs_w = sym.collect(rhs_w, x)
#print("rhs_w", rhs_w, "\n")

#dxdxflux1_nw = -sym.diff(relative_permeability[1]['nonwetting'](S_pc_rel_sym[1](p1_nw - p1_w))*sym.diff(p1_nw, x, 1), x, 1)
#dydyflux1_nw = -sym.diff(relative_permeability[1]['nonwetting'](S_pc_rel_sym[1](p1_nw - p1_w))*sym.diff(p1_nw, y, 1), y, 1)
dxdxflux1_nw = -1/viscosity[1]['nonwetting']*sym.diff(relative_permeability[1]['nonwetting'](1-S_pc_rel[1](p1_nw - p1_w))*sym.diff(p1_nw, x, 1), x, 1)
dydyflux1_nw = -1/viscosity[1]['nonwetting']*sym.diff(relative_permeability[1]['nonwetting'](1-S_pc_rel[1](p1_nw - p1_w))*sym.diff(p1_nw, y, 1), y, 1)

rhs1_nw = dtS1_nw + dxdxflux1_nw + dydyflux1_nw
rhs1_nw = sym.printing.ccode(rhs1_nw)
print("rhs_nw = ", rhs1_nw, "\n")

## subdomain2
p2_w = -20 - (1+t*t)*(1 + x**2)
p2_nw = -t*(1 + x**2)**2 - sym.sqrt(2+t**2)*(1 + x**2)


#dtS2_w = sym.diff(S_pc_rel_sym[2](p2_nw - p2_w), t, 1)
#dtS2_nw = -sym.diff(S_pc_rel_sym[2](p2_nw - p2_w), t, 1)
dtS2_w = porosity[2]*sym.diff(S_pc_rel[2](p2_nw - p2_w), t, 1)
dtS2_nw = -porosity[2]*sym.diff(S_pc_rel[2](p2_nw - p2_w), t, 1)
print("dtS2_w = ", dtS2_w, "\n")
print("dtS2_nw = ", dtS2_nw, "\n")

#dxdxflux2_w = -sym.diff(relative_permeability[2]['wetting'](S_pc_rel_sym[2](p2_nw - p2_w))*sym.diff(p2_w, x, 1), x, 1)
#dydyflux2_w = -sym.diff(relative_permeability[2]['wetting'](S_pc_rel_sym[2](p2_nw - p2_w))*sym.diff(p2_w, y, 1), y, 1)
dxdxflux2_w = -1/viscosity[2]['wetting']*sym.diff(relative_permeability[2]['wetting'](S_pc_rel[2](p2_nw - p2_w))*sym.diff(p2_w, x, 1), x, 1)
dydyflux2_w = -1/viscosity[2]['wetting']*sym.diff(relative_permeability[2]['wetting'](S_pc_rel[2](p2_nw - p2_w))*sym.diff(p2_w, y, 1), y, 1)

rhs2_w = dtS2_w + dxdxflux2_w + dydyflux2_w
rhs2_w = sym.printing.ccode(rhs2_w)
print("rhs2_w = ", rhs2_w, "\n")
#rhs_w = sym.expand(rhs_w)
#print("rhs_w", rhs_w, "\n")
#rhs_w = sym.collect(rhs_w, x)
#print("rhs_w", rhs_w, "\n")

#dxdxflux2_nw = -sym.diff(relative_permeability[2]['nonwetting'](S_pc_rel_sym[2](p2_nw - p2_w))*sym.diff(p2_nw, x, 1), x, 1)
#dydyflux2_nw = -sym.diff(relative_permeability[2]['nonwetting'](S_pc_rel_sym[2](p2_nw - p2_w))*sym.diff(p2_nw, y, 1), y, 1)
dxdxflux2_nw = -1/viscosity[2]['nonwetting']*sym.diff(relative_permeability[2]['nonwetting'](1-S_pc_rel[2](p2_nw - p2_w))*sym.diff(p2_nw, x, 1), x, 1)
dydyflux2_nw = -1/viscosity[2]['nonwetting']*sym.diff(relative_permeability[2]['nonwetting'](1-S_pc_rel[2](p2_nw - p2_w))*sym.diff(p2_nw, y, 1), y, 1)

rhs2_nw = dtS2_nw + dxdxflux2_nw + dydyflux2_nw
rhs2_nw = sym.printing.ccode(rhs2_nw)
print("rhs2_nw = ", rhs2_nw, "\n")


###############################################################################

source_expression = {
    1: {'wetting': rhs1_w,
        'nonwetting': rhs1_nw},
    2: {'wetting': rhs2_w,
        'nonwetting': rhs2_nw}
}

p1_w_00 = p1_w.subs(t, 0)
p1_nw_00 = p1_nw.subs(t, 0)
p2_w_00 = p2_w.subs(t, 0)
p2_nw_00 = p2_nw.subs(t, 0)
# p1_w_00 = sym.printing.ccode(p1_w_00)

initial_condition = {
    1: {'wetting': sym.printing.ccode(p1_w_00),
        'nonwetting': sym.printing.ccode(p1_nw_00)},#
    2: {'wetting': sym.printing.ccode(p2_w_00),
        'nonwetting': sym.printing.ccode(p2_nw_00)}
}

exact_solution = {
    1: {'wetting': sym.printing.ccode(p1_w),
        'nonwetting': sym.printing.ccode(p1_nw)},#
    2: {'wetting': sym.printing.ccode(p2_w),
        'nonwetting': sym.printing.ccode(p2_nw)}
}

# similary to the outer boundary dictionary, if a patch has no outer boundary
# None should be written instead of an expression. This is a bit of a brainfuck:
# dirichletBC[ind] gives a dictionary of the outer boundaries of subdomain ind.
# Since a domain patch can have several disjoint outer boundary parts, the expressions
# need to get an enumaration index which starts at 0. So dirichletBC[ind][j] is
# the dictionary of outer dirichlet conditions of subdomain ind and boundary part j.
# finally, dirichletBC[ind][j]['wetting'] and dirichletBC[ind][j]['nonwetting'] return
# the actual expression needed for the dirichlet condition for both phases if present.
dirichletBC = {
#subdomain index: {outer boudary part index: {phase: expression}}
    1: { 0: {'wetting': sym.printing.ccode(p1_w),
             'nonwetting': sym.printing.ccode(p1_nw)}},
    2: { 0: {'wetting': sym.printing.ccode(p2_w),
             'nonwetting': sym.printing.ccode(p2_nw)}}
}

# def saturation(pressure, subdomain_index):
#     # inverse capillary pressure-saturation-relationship
#     return df.conditional(pressure < 0, 1/((1 - pressure)**(1/(subdomain_index + 1))), 1)
#
# sa

write_to_file = {
    'meshes_and_markers': True,
    'L_iterations': True
}


# initialise LDD simulation class
simulation = ldd.LDDsimulation(tol = 1E-14)
simulation.set_parameters(output_dir = "./output/",#
    subdomain_def_points = subdomain_def_points,#
    isRichards = isRichards,#
    interface_def_points = interface_def_points,#
    outer_boundary_def_points = outer_boundary_def_points,#
    adjacent_subdomains = adjacent_subdomains,#
    mesh_resolution = mesh_resolution,#
    viscosity = viscosity,#
    porosity = porosity,#
    L = L,#
    lambda_param = lambda_param,#
    relative_permeability = relative_permeability,#
    saturation = S_pc_rel,#
    starttime = starttime,#
    number_of_timesteps = number_of_timesteps,
    number_of_timesteps_to_analyse = number_of_timesteps_to_analyse,
    timestep_size = timestep_size,#
    sources = source_expression,#
    initial_conditions = initial_condition,#
    dirichletBC_expression_strings = dirichletBC,#
    exact_solution = exact_solution,#
    write2file = write_to_file,#
    )

simulation.initialise()
# simulation.write_exact_solution_to_xdmf()
simulation.run()
