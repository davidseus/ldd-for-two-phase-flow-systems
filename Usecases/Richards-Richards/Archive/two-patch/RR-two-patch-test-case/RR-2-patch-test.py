#!/usr/bin/python3
"""RR 2 patch LDDsimulation, Copyright 2020, David Seus

This program runs an LDD simulation on a two-domain substructuring using
a Richards-Richards coupling.

# LICENCE #####################################################################
Copyright 2020, David Seus
david.seus[at]ians.uni-stuttgart.de
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
"""
import dolfin as df
import mshr
import numpy as np
import sympy as sym
import typing as tp
import domainPatch as dp
import LDDsimulation as ldd
import functools as ft
import helpers as hlp
#import ufl as ufl

use_case = "RR-two-patch"
solver_tol = 1E-6

############ GRID #######################ü
mesh_resolution = 40
timestep_size = 0.001
number_of_timesteps = 1500
# decide how many timesteps you want analysed. Analysed means, that we write out
# subsequent errors of the L-iteration within the timestep.
number_of_timesteps_to_analyse = 10
starttime = 0

Lw = 0.25 #/timestep_size
Lnw=Lw

lambda_w = 4

include_gravity = True
debugflag = False
analyse_condition = True

output_string = "./output/new_gravity_term-number_of_timesteps{}_".format(number_of_timesteps)

##### Domain and Interface ####
# global simulation domain domain
sub_domain0_vertices = [df.Point(-1.0,-1.0), #
                        df.Point(1.0,-1.0),#
                        df.Point(1.0,1.0),#
                        df.Point(-1.0,1.0)]
# interface between subdomain1 and subdomain2
interface12_vertices = [df.Point(-1.0, 0.0),
                        df.Point(1.0, 0.0) ]
# subdomain1.
sub_domain1_vertices = [interface12_vertices[0],
                        interface12_vertices[1],
                        sub_domain0_vertices[2],
                        sub_domain0_vertices[3] ]

# vertex coordinates of the outer boundaries. If it can not be specified as a
# polygon, use an entry per boundary polygon. This information is used for defining
# the Dirichlet boundary conditions. If a domain is completely internal, the
# dictionary entry should be 0: None
subdomain1_outer_boundary_verts = {
    0: [interface12_vertices[1],
        sub_domain0_vertices[2],
        sub_domain0_vertices[3], #
        interface12_vertices[0]]
}
# subdomain2
sub_domain2_vertices = [sub_domain0_vertices[0],
                        sub_domain0_vertices[1],
                        interface12_vertices[1],
                        interface12_vertices[0] ]

subdomain2_outer_boundary_verts = {
    0: [interface12_vertices[0], #
        sub_domain0_vertices[0],
        sub_domain0_vertices[1],
        interface12_vertices[1]]
}

# subdomain2_outer_boundary_verts = {
#     0: [interface12_vertices[0], df.Point(0.0,0.0)],#
#     1: [df.Point(0.0,0.0), df.Point(1.0,0.0)], #
#     2: [df.Point(1.0,0.0), interface12_vertices[1]]
# }
# subdomain2_outer_boundary_verts = {
#     0: None
# }

# list of subdomains given by the boundary polygon vertices.
# Subdomains are given as a list of dolfin points forming
# a closed polygon, such that mshr.Polygon(subdomain_def_points[i]) can be used
# to create the subdomain. subdomain_def_points[0] contains the
# vertices of the global simulation domain and subdomain_def_points[i] contains the
# vertices of the subdomain i.
subdomain_def_points = [sub_domain0_vertices,#
                      sub_domain1_vertices,#
                      sub_domain2_vertices]
# in the below list, index 0 corresponds to the 12 interface which has index 1
interface_def_points = [interface12_vertices]

# if a subdomain has no outer boundary write None instead, i.e.
# i: None
# if i is the index of the inner subdomain.
outer_boundary_def_points = {
    # subdomain number
    1 : subdomain1_outer_boundary_verts,
    2 : subdomain2_outer_boundary_verts
}

# adjacent_subdomains[i] contains the indices of the subdomains sharing the
# interface i (i.e. given by interface_def_points[i]).
adjacent_subdomains = [[1,2]]
isRichards = {
    1: True, #
    2: True
    }


viscosity = {#
# subdom_num : viscosity
    1 : {'wetting' :1}, #
    2 : {'wetting' :1}
}

densities = {#
# subdom_num : viscosity
    1 : {'wetting' :1}, #
    2 : {'wetting' :1}
}

# gravity_acceleration = 9.81

porosity = {#
# subdom_num : porosity
    1 : 1,#
    2 : 1
}

L = {#
# subdom_num : subdomain L for L-scheme
    1 : {'wetting': Lw},#
    2 : {'wetting': Lw}
}

lambda_param = {#
# subdom_num : lambda parameter for the L-scheme
    1 : {'wetting': lambda_w},#
    2 : {'wetting': lambda_w}
}

## relative permeabilty functions on subdomain 1
def rel_perm1(s):
    # relative permeabilty on subdomain1
    return s**2

_rel_perm1 = ft.partial(rel_perm1)

subdomain1_rel_perm = {
    'wetting': _rel_perm1,#
    'nonwetting': None
}
## relative permeabilty functions on subdomain 2
def rel_perm2(s):
    # relative permeabilty on subdomain2
    return s**3

_rel_perm2 = ft.partial(rel_perm2)

subdomain2_rel_perm = {
    'wetting': _rel_perm2,#
    'nonwetting': None
}

## dictionary of relative permeabilties on all domains.
relative_permeability = {#
    1: subdomain1_rel_perm,
    2: subdomain2_rel_perm
}

# this function needs to be monotonically decreasing in the capillary_pressure.
# since in the richards case pc = -pw, this becomes as a function of pw a monotonically
# INCREASING function like in our Richards-Richards paper. However, since we unify
# the treatment in the code for Richards and two-phase, we need the same requierment
# for both cases, two-phase and Richards.
def saturation(capillary_pressure, subdomain_index):
    # inverse capillary pressure-saturation-relationship
    return df.conditional(capillary_pressure > 0, 1/((1 + capillary_pressure)**(1/(subdomain_index + 1))), 1)

sat_pressure_relationship = {#
    1: ft.partial(saturation, subdomain_index = 1),#
    2: ft.partial(saturation, subdomain_index = 2)
}

source_expression = {
    1: {'wetting': '4.0/pow(1 + x[0]*x[0] + x[1]*x[1], 2) - t/sqrt( pow(1 + t*t, 3)*(1 + x[0]*x[0] + x[1]*x[1]) )'},
    2: {'wetting': '2.0*(1-x[0]*x[0])/pow(1 + x[0]*x[0], 2) - 2*t/(3*pow( pow(1 + t*t, 4)*(1 + x[0]*x[0]), 1/3))'}
}

initial_condition = {
    1: {'wetting': '-(x[0]*x[0] + x[1]*x[1])'},#
    2: {'wetting': '-x[0]*x[0]'}
}

exact_solution = {
    1: {'wetting': '1.0 - (1.0 + t*t)*(1.0 + x[0]*x[0] + x[1]*x[1])'},#
    2: {'wetting': '1.0 - (1.0 + t*t)*(1.0 + x[0]*x[0])'}
}

# similary to the outer boundary dictionary, if a patch has no outer boundary
# None should be written instead of an expression. This is a bit of a brainfuck:
# dirichletBC[ind] gives a dictionary of the outer boundaries of subdomain ind.
# Since a domain patch can have several disjoint outer boundary parts, the expressions
# need to get an enumaration index which starts at 0. So dirichletBC[ind][j] is
# the dictionary of outer dirichlet conditions of subdomain ind and boundary part j.
# finally, dirichletBC[ind][j]['wetting'] and dirichletBC[ind][j]['nonwetting'] return
# the actual expression needed for the dirichlet condition for both phases if present.
dirichletBC = {
#subdomain index: {outer boudary part index: {phase: expression}}
    1: { 0: {'wetting': exact_solution[1]['wetting']}},
    2: { 0: {'wetting': exact_solution[2]['wetting']}}
}

# def saturation(pressure, subdomain_index):
#     # inverse capillary pressure-saturation-relationship
#     return df.conditional(pressure < 0, 1/((1 - pressure)**(1/(subdomain_index + 1))), 1)
#
# sa

write_to_file = {
    'meshes_and_markers': True,
    'L_iterations': True
}

# initialise LDD simulation class
simulation = ldd.LDDsimulation(tol = 1E-14, debug = debugflag, LDDsolver_tol=solver_tol)
simulation.set_parameters(use_case=use_case,
    output_dir = output_string,#
    subdomain_def_points = subdomain_def_points,#
    isRichards = isRichards,#
    interface_def_points = interface_def_points,#
    outer_boundary_def_points = outer_boundary_def_points,#
    adjacent_subdomains = adjacent_subdomains,#
    mesh_resolution = mesh_resolution,#
    viscosity = viscosity,#
    porosity = porosity,#
    L = L,#
    lambda_param = lambda_param,#
    relative_permeability = relative_permeability,#
    saturation = sat_pressure_relationship,#
    starttime = starttime,#
    number_of_timesteps = number_of_timesteps,
    number_of_timesteps_to_analyse = number_of_timesteps_to_analyse,
    timestep_size = timestep_size,#
    sources = source_expression,#
    initial_conditions = initial_condition,#
    dirichletBC_expression_strings = dirichletBC,#
    exact_solution = exact_solution,#
    densities = densities,#
    include_gravity = include_gravity,#
    write2file = write_to_file,#
    )

simulation.initialise()
#print(simulation.__dict__)
simulation.run(analyse_condition=analyse_condition)
# simulation.LDDsolver(time = 0, debug = True, analyse_timestep = True)
# df.info(parameters, True)
