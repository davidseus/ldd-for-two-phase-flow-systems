# Meshes 
The mesh data in the various folders are only saved here to be able to plot the 
data either with `paraview` or the script found in the folder `Plotscript`.
They are produced by the simulation class as output and are not reused.  
