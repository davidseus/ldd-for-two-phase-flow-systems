# LDD simulation for two-phase flow systems (R–R, TP–TP, TP–R coupling)
This project provides a Fenics based 2D domain decomposition code implementing an LDD solver for two-phase flow systems that 
is part of the author's thesis [LDD schemes for two-phase flow systems](https://www.dr.hut-verlag.de/978-3-8439-4874-6.html).
Two-phase flow systems means, that each subdomain can either adopt the full two-phase flow equations or the Richards/Richardson equation. 
Flexible domain substructurings are possible so long as each subdomain has polygonal boundary. 

## How to get started
To use this module, start by cloning the repository by running 

    git clone https://gitlab.com/davidseus/ldd-for-two-phase-flow-systems.git

The folder [Setup](./Setup) instructions contains instructions on how to get `Fenics` up and running inside a docker container. 

After following the steps decsribed in [Setup](./Setup), have a look at the folder [Usecases](./Usecases) where several usecases for this module can be found. 
Each of the usecases should be runnable out of the box and produce results. Adjust the files as needed. 
Most likely, you will want to create your own usecases featuring other data fuctions, substructurings and so on. 
Information on how to create your own usecases can be found in the folder [Usecases](./Usecases). 

To get an idea of the types of substructurings that are possible with this code have a look at [Meshes](./Meshes) and open some of the 
marker function `domain_marker_meshrez*.pvd` or `interface_marker_global_meshrez*.pvd`.

## State of development
### Curent state
Currently the code can do the following:

- run a flexible combination of models, i.e. each subdomain can assume Richards/Richardson's equation or the full two-phase flow equations. 

- run examples with exact solutions and compare the numerical solution to it. 

- run mesh studies against for these exact solution examples. 

### TODO

- Parallelisation: Unfortunately atm the code does not run in parallel, although the different mesh sizes during mesh study calculations do run in parallel. The code is quite capsuled already, but the method `domainPatch.calc_gli_term()` in `./LDDsimulation/domainPatch.py` needs to be adapted if one were to parallelise the code. Mainly, one needs to figure out how to share pointers to the storages `domainPatch.pressure[phase]` and 
`domainPatch.pressure_prev_timestep[phase]` between processes. Hopefully this is possible without copying the data that is used internally in `Fenics`
into `numpy arrays` each time.

- more modells would be nice, maybe more effects. 

- more examples without exact solutions. Some Code needs to be written to compare a numerical solution to one with a finer meshrez.


## License
`LDDsimulation` is licensed under the terms and conditions of the GNU General
Public License (GPL) version 3 or - at your option - any later
version. The GPL can be [read online](https://www.gnu.org/licenses/gpl-3.0.en.html) or in the [COPYING](./COPYING) file
provided in the topmost directory of the `LDDsimulation` source code tree.
